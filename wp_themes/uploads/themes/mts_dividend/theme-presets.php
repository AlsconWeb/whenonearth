<?php
// make sure to not include translations
$args['presets']['default'] = array(
	'title' => 'Default',
	'demo' => 'http://demo.mythemeshop.com/dividend/',
	'thumbnail' => get_template_directory_uri().'/options/demo-importer/demo-files/default/thumb.jpg', // could use external url, to minimize theme zip size
	'menus' => array( 'primary-menu' => 'Menu', 'footer-menu' => 'Footer' ), // menu location slug => Demo menu name
	'options' => array( 'show_on_front' => 'posts', 'posts_per_page' => 5 ),
);

$args['presets']['deals'] = array(
	'title' => 'Deals',
	'demo' => 'http://demo.mythemeshop.com/dividend-deals/',
	'thumbnail' => get_template_directory_uri().'/options/demo-importer/demo-files/deals/thumb.jpg', // could use external url, to minimize theme zip size
	'menus' => array( 'primary-menu' => 'Menu' ), // menu location slug => Demo menu name
	'options' => array( 'show_on_front' => 'page', 'page_on_front' => '2258', 'page_for_posts' => '2259', 'posts_per_page' => 8 ),
);

$args['presets']['magazine'] = array(
	'title' => 'Magazine',
	'demo' => 'http://demo.mythemeshop.com/dividend-magazine/',
	'thumbnail' => get_template_directory_uri().'/options/demo-importer/demo-files/magazine/thumb.jpg', // could use external url, to minimize theme zip size
	'menus' => array( 'primary-menu' => 'Menu' ), // menu location slug => Demo menu name
	'options' => array( 'show_on_front' => 'posts', 'posts_per_page' => 21 ),
);

$args['presets']['minimal'] = array(
	'title' => 'Minimal',
	'demo' => 'http://demo.mythemeshop.com/dividend-minimal/',
	'thumbnail' => get_template_directory_uri().'/options/demo-importer/demo-files/minimal/thumb.jpg', // could use external url, to minimize theme zip size
	'menus' => array( 'primary-menu' => 'Menu', 'footer-menu' => 'Footer' ), // menu location slug => Demo menu name
	'options' => array( 'show_on_front' => 'posts', 'posts_per_page' => 9 ),
);

global $mts_presets;
$mts_presets = $args['presets'];
