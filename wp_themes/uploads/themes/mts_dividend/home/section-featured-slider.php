<?php $mts_options = get_option(MTS_THEME_NAME); ?>
<div class="featuredBox">
    <?php $i = 1;
    $flag = false;
    // prevent implode error
    if ( empty( $mts_options['mts_full_featured_slider_cat'] ) || !is_array( $mts_options['mts_full_featured_slider_cat'] ) ) {
        $mts_options['mts_full_featured_slider_cat'] = array('0');
    }
    if ( is_active_sidebar( 'widget-subscribe' ) ) {
        $flag = true;
    }
    $slider_cat = implode( ",", $mts_options['mts_full_featured_slider_cat'] );
    $slider_query = new WP_Query('cat='.$slider_cat.'&posts_per_page=4&ignore_sticky_posts=1'); 
    while ($slider_query->have_posts()) : $slider_query->the_post();
        if($i == 1){ ?> 
            <div class="firstpost excerpt">
                <a href="<?php echo esc_url( get_the_permalink() ); ?>" title="<?php echo esc_attr( get_the_title() ); ?>" id="first-thumbnail" class="featuredPost">
                    <div class="featured-thumbnail" style="background:url(<?php echo mts_get_thumbnail_url( 'dividend-bigthumb' ); ?>); min-height: 500px; background-position: center center; "></div> 
                    <header>
                        <?php $category = get_the_category();  if(!empty($category)){ ?>
                            <div class="thecategory"><?php echo $category[0]->cat_name; ?></div>
                        <?php } ?>
                        <h2 class="title front-view-title"><?php the_title(); ?></h2>
                    </header>
                </a>
            </div><!--.post excerpt-->
        <?php } elseif($i == 2) { ?>
            <div class="secondpost excerpt">
                <a href="<?php echo esc_url( get_the_permalink() ); ?>" title="<?php echo esc_attr( get_the_title() ); ?>" id="second-thumbnail" class="featuredPost">
                    <div class="featured-thumbnail" style="background:url(<?php echo mts_get_thumbnail_url( 'dividend-mediumthumb' ); ?>); min-height: 245px; background-position: center center; "></div> 
                    <header>
                        <?php $category = get_the_category();  if(!empty($category)){ ?>
                            <div class="thecategory"><?php echo $category[0]->cat_name; ?></div>
                        <?php } ?>
                        <h2 class="title front-view-title"><?php the_title(); ?></h2>
                    </header>
                </a>
            </div><!--.post excerpt-->
        <?php } elseif($i == 3) { ?>
            <div class="thirdpost excerpt">
                <a href="<?php echo esc_url( get_the_permalink() ); ?>" title="<?php echo esc_attr( get_the_title() ); ?>" id="third-thumbnail" class="featuredPost">
                    <div class="featured-thumbnail" style="background:url(<?php echo mts_get_thumbnail_url( 'dividend-smallthumb' ); ?>); min-height: 250px; background-position: center center; "></div> 
                    <header>
                        <?php $category = get_the_category();  if(!empty($category)){ ?>
                            <div class="thecategory"><?php echo $category[0]->cat_name; ?></div>
                        <?php } ?>
                        <h2 class="title front-view-title"><?php the_title(); ?></h2>
                    </header>
                </a>
            </div><!--.post excerpt-->
        <?php } elseif($i == 4) { ?>
            <div class="fourthpost excerpt">
                <?php if($flag == true) { ?>
                    <div id="subscribe" class="subscribe-container">
                        <?php dynamic_sidebar( 'widget-subscribe' ); ?>
                    </div>
                <?php } else { ?>
                    <a href="<?php echo esc_url( get_the_permalink() ); ?>" title="<?php echo esc_attr( get_the_title() ); ?>" id="third-thumbnail" class="featuredPost">
                        <div class="featured-thumbnail" style="background:url(<?php echo mts_get_thumbnail_url( 'dividend-smallthumb' ); ?>); min-height: 250px; background-position: center center; width: 437px;"></div> 
                        <header>
                            <?php $category = get_the_category();  if(!empty($category)){ ?>
                                <div class="thecategory"><?php echo $category[0]->cat_name; ?></div>
                            <?php } ?>
                            <h2 class="title front-view-title"><?php the_title(); ?></h2>
                        </header>
                    </a>
                <?php } ?>
            </div><!--.post excerpt-->
        <?php }
    $i++; endwhile; wp_reset_query(); ?> 
</div>