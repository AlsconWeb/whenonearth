<?php $mts_options = get_option(MTS_THEME_NAME); ?>
<div class="homepage-deals clearfix">
	<?php $mts_deals_carousel_title = $mts_options['mts_deals_carousel_title'];
		  $mts_deals_num    = empty ( $mts_options['mts_deals_count_home'] ) ? '6' : $mts_options['mts_deals_count_home']; ?>
	<div class="container">
		<?php if ( !empty( $mts_deals_carousel_title ) ) { ?>
			<h3 class="featured-category-title"><a href="<?php echo get_post_type_archive_link( 'deals' ); ?>"><?php echo $mts_deals_carousel_title; ?></a></h3>
		<?php }?>
		<a href="<?php echo get_post_type_archive_link( 'deals' ); ?>" class="btn-archive-link"><?php _e( 'View all deals', 'dividend' ); ?></a>
		<div class="deals-carousel-container clearfix loading">
			<div id="slider" class="deals-carousel">
				<?php
				$deals_query = new WP_Query();
				$deals_query->query('post_type=deals&ignore_sticky_posts=1&posts_per_page='.$mts_deals_num);
				while ( $deals_query->have_posts() ) : $deals_query->the_post();
				?>
				<div class="deals-carousel-item"> 
					<a href="<?php echo esc_url( get_the_permalink() ); ?>" class="deals-carousel-post">
						<?php the_post_thumbnail('dividend-dealscarousel',array('title' => '')); ?>
						<div class="deals-caption">
							<h2 class="deals-title"><?php the_title(); ?></h2>
							<div class="deals-link"><?php _e( 'View Deal', 'dividend' ); ?></div>
						</div>
					</a> 
				</div>
				<?php endwhile; wp_reset_postdata(); ?>
			</div>
		</div>
	</div>	
</div>	