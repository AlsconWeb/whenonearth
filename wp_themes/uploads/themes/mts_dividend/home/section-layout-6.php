<?php $mts_options = get_option(MTS_THEME_NAME); ?>

<?php global $j, $post_color; ?>

<article class="latestPost excerpt">
	<div class="latestPost-layout" style="color: <?php echo $post_color; ?>">
		<a href="<?php echo esc_url( get_the_permalink() ); ?>" title="<?php echo esc_attr( get_the_title() ); ?>" class="post-image post-image-left"><?php echo '<div class="featured-thumbnail">'; the_post_thumbnail('dividend-featured',array('title' => '')); echo '</div>';
			if (function_exists('wp_review_show_total')) wp_review_show_total(true, 'latestPost-review-wrapper'); ?>
			<div class="article-content">
				<header>
					<h2 class="title front-view-title"><?php the_title(); ?></h2>
				</header>
			</div>
		</a>
	</div>
</article>