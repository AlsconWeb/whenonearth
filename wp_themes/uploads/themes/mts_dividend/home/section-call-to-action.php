<?php $mts_options = get_option(MTS_THEME_NAME); ?>
<div class="call-to-action">
	<div class="call-to-action-left">
		<?php if ( !empty( $mts_options['mts_call_to_action_small_text'] ) ) { ?>
			<div class="call-to-action-text"><?php echo $mts_options['mts_call_to_action_small_text']; ?></div>
		<?php } 
		if ( !empty( $mts_options['mts_call_to_action_title'] ) ) { ?>
			<div class="call-to-action-title"><?php echo $mts_options['mts_call_to_action_title']; ?></div>
		<?php }
		if ( !empty( $mts_options['mts_call_to_action_button_link'] ) ) { ?>
			<div class="call-to-action-button">
				<a href="<?php echo esc_url( $mts_options['mts_call_to_action_button_link'] ); ?>"><?php echo $mts_options['mts_call_to_action_button_text']; ?><svg baseProfile="tiny" height="52px" version="1.2" viewBox="0 0 24 24" width="52px" fill="#ffffff" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Layer_1"><path d="M13.293,7.293c-0.391,0.391-0.391,1.023,0,1.414L15.586,11H8c-0.552,0-1,0.448-1,1s0.448,1,1,1h7.586l-2.293,2.293   c-0.391,0.391-0.391,1.023,0,1.414C13.488,16.902,13.744,17,14,17s0.512-0.098,0.707-0.293L19.414,12l-4.707-4.707   C14.316,6.902,13.684,6.902,13.293,7.293z"/></path></g></svg> </a>
			</div>
	   	<?php } ?>
   	</div>
	<img src="<?php echo $mts_options['mts_call_to_action_image']; ?>" width="547" height="410">
</div>