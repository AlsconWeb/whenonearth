<?php
/**
 * The template for displaying Deals archive.
 */
$mts_options = get_option(MTS_THEME_NAME);

get_header();

if ( !is_paged() && $mts_options['mts_deals_featured_slider'] == '1' ) { //Featured Slider Section ?>
	<div class="featuredBox deals-featured">
	    <?php $i = 1;
	    $flag = false;
	    if ( is_active_sidebar( 'widget-deals-subscribe' ) ) {
	        $flag = true;
	    }
	    $args = array(
		    'post_type' => 'deals',
		    'ignore_sticky_posts' => 1,
		    'posts_per_page' => 4,
		    'tax_query' => array(
		        array(
		            'taxonomy' => 'mts_categories',
		            'terms' => !empty($mts_options['mts_deals_featured_slider_cat']) ? $mts_options['mts_deals_featured_slider_cat'] : ' '

		        ),
		    ),
		);
	    $deals_query = new WP_Query($args);
	    while ($deals_query->have_posts()) : $deals_query->the_post();
	        if($i == 1){ ?> 
	            <?php $deals_button = get_post_meta( get_the_ID(), 'mts_deals_button', true );
		        $deals_button_url = get_post_meta( get_the_ID(), 'mts_deals_button_url', true ); ?>
	            <div class="firstpost excerpt">
	                <div class="featured-thumbnail" style="background:url(<?php echo mts_get_thumbnail_url( 'dividend-bigthumb' ); ?>); min-height: 500px; background-position: center center;">
		                <header>
		                    <a href="<?php echo esc_url( get_the_permalink() ); ?>" title="<?php echo esc_attr( get_the_title() ); ?>">
		                    	<h2 class="title front-view-title"><?php the_title(); ?></h2>	                    
			                    <div class="deals-button-wrap">
				                    <div class="deals-button">
				                    	<?php echo $deals_button; ?><svg baseProfile="tiny" height="48px" version="1.2" viewBox="0 0 24 24" width="48px" fill="#ffffff" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Layer_1"><path d="M13.293,7.293c-0.391,0.391-0.391,1.023,0,1.414L15.586,11H8c-0.552,0-1,0.448-1,1s0.448,1,1,1h7.586l-2.293,2.293   c-0.391,0.391-0.391,1.023,0,1.414C13.488,16.902,13.744,17,14,17s0.512-0.098,0.707-0.293L19.414,12l-4.707-4.707   C14.316,6.902,13.684,6.902,13.293,7.293z"/></path></g></svg> 
				                    </div>	
			                    </div>
		                    </a>
		                </header>	
	                </div>
	            </div><!--.post excerpt-->
	        <?php } elseif($i == 2) { ?>
	            <?php $deals_button = get_post_meta( get_the_ID(), 'mts_deals_button', true );
		        $deals_button_url = get_post_meta( get_the_ID(), 'mts_deals_button_url', true ); ?>
	            <div class="secondpost excerpt">
	                <div class="featured-thumbnail" style="background:url(<?php echo mts_get_thumbnail_url( 'dividend-mediumthumb' ); ?>); min-height: 245px; background-position: center center;">
	                    <header>
		                    <a href="<?php echo esc_url( get_the_permalink() ); ?>" title="<?php echo esc_attr( get_the_title() ); ?>">
			                    <h2 class="title front-view-title"><?php the_title(); ?></h2>
			                    <div class="deals-button-wrap">
				                    <div class="deals-button">
				                    	<?php echo $deals_button; ?><svg baseProfile="tiny" height="40px" version="1.2" viewBox="0 0 24 24" width="40px" fill="#ffffff" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Layer_1"><path d="M13.293,7.293c-0.391,0.391-0.391,1.023,0,1.414L15.586,11H8c-0.552,0-1,0.448-1,1s0.448,1,1,1h7.586l-2.293,2.293   c-0.391,0.391-0.391,1.023,0,1.414C13.488,16.902,13.744,17,14,17s0.512-0.098,0.707-0.293L19.414,12l-4.707-4.707   C14.316,6.902,13.684,6.902,13.293,7.293z"/></path></g></svg> 
				                    </div>	
				                </div>    
		                    </a>
		                </header>	
	                </div>    
	            </div><!--.post excerpt-->
	        <?php } elseif($i == 3) { ?>
	            <?php $deals_button = get_post_meta( get_the_ID(), 'mts_deals_button', true );
		        $deals_button_url = get_post_meta( get_the_ID(), 'mts_deals_button_url', true ); ?>
	            <div class="thirdpost excerpt">
	                <div class="featured-thumbnail" style="background:url(<?php echo mts_get_thumbnail_url( 'dividend-smallthumb' ); ?>); min-height: 250px; background-position: center center;">
	                    <header>
		                    <a href="<?php echo esc_url( get_the_permalink() ); ?>" title="<?php echo esc_attr( get_the_title() ); ?>">
			                    <h2 class="title front-view-title"><?php the_title(); ?></h2>
			                    <div class="deals-button-wrap">
				                    <div class="deals-button">
				                    	<?php echo $deals_button; ?><svg baseProfile="tiny" height="40px" version="1.2" viewBox="0 0 24 24" width="40px" fill="#ffffff" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Layer_1"><path d="M13.293,7.293c-0.391,0.391-0.391,1.023,0,1.414L15.586,11H8c-0.552,0-1,0.448-1,1s0.448,1,1,1h7.586l-2.293,2.293   c-0.391,0.391-0.391,1.023,0,1.414C13.488,16.902,13.744,17,14,17s0.512-0.098,0.707-0.293L19.414,12l-4.707-4.707   C14.316,6.902,13.684,6.902,13.293,7.293z"/></path></g></svg> 
				                    </div>	
			                    </div>
		                    </a>	
		                </header>	
	                </div>
	            </div><!--.post excerpt-->
	        <?php } elseif($i == 4) { ?>
	            <div class="fourthpost excerpt">
	                <?php if($flag == true) { ?>
	                    <div id="subscribe" class="subscribe-container">
	                        <?php dynamic_sidebar( 'widget-deals-subscribe' ); ?>
	                    </div>
	                <?php } else { ?>
	                    <?php $deals_button = get_post_meta( get_the_ID(), 'mts_deals_button', true );
		            	$deals_button_url = get_post_meta( get_the_ID(), 'mts_deals_button_url', true ); ?>
	                    <div class="featured-thumbnail" style="background:url(<?php echo mts_get_thumbnail_url( 'dividend-smallthumb' ); ?>); min-height: 250px; background-position: center center;">
		                    <header>
			                    <a href="<?php echo esc_url( get_the_permalink() ); ?>" title="<?php echo esc_attr( get_the_title() ); ?>">
				                    <h2 class="title front-view-title"><?php the_title(); ?></h2>
				                    <div class="deals-button-wrap">
					                    <div class="deals-button">
					                    	<?php echo $deals_button; ?><svg baseProfile="tiny" height="40px" version="1.2" viewBox="0 0 24 24" width="40px" fill="#ffffff" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Layer_1"><path d="M13.293,7.293c-0.391,0.391-0.391,1.023,0,1.414L15.586,11H8c-0.552,0-1,0.448-1,1s0.448,1,1,1h7.586l-2.293,2.293   c-0.391,0.391-0.391,1.023,0,1.414C13.488,16.902,13.744,17,14,17s0.512-0.098,0.707-0.293L19.414,12l-4.707-4.707   C14.316,6.902,13.684,6.902,13.293,7.293z"/></path></g></svg> 
					                    </div>
					                </div>    
			                    </a>	
			                </header>	
		                </div>
	                <?php } ?>
	            </div><!--.post excerpt-->
	        <?php }
	    $i++; endwhile; wp_reset_query(); ?> 
	</div>
<?php } ?>	

<div id="page" class="archive-deals">
	<div id="deals-archive">
		<div class="deals-posts">
			<?php $j = 0; if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<article class="latestPost excerpt deals-related-post">
				<div class="deals-layout">
				<?php $deals_expiry_date = get_post_meta( get_the_ID(), 'mts_deals_expire'); 
				$deals_no_expiry_date = get_post_meta( get_the_ID(), 'mts_deals_no_expire_date'); 
				$deals_button = get_post_meta( get_the_ID(), 'mts_deals_button', true );
	            $deals_button_url = get_post_meta( get_the_ID(), 'mts_deals_button_url', true ); 
				$deals_featured_text = get_post_meta( get_the_ID(), 'mts_deals_featured_text', true ); ?>
					<a href="<?php echo esc_url( get_the_permalink() ); ?>" title="<?php echo esc_attr( get_the_title() ); ?>">
						<?php echo '<div class="deals-thumbnail">'; the_post_thumbnail('dividend-deals', array('title' => '')); echo '</div>'; ?>
					</a>
					<div class="content-container">
						<?php if( !empty($deals_expiry_date) && empty($deals_no_expiry_date)) : ?>
							<div class="deals-related-expiry">
								<div class="deals-related-expirydate">
									<?php
									$now = new DateTime(current_time('mysql'));
									$ref = new DateTime($deals_expiry_date[0]);
									$diff = $now->diff($ref);

									if ( $diff->invert ) {
										if ( $diff->days == 1 ) {
											_e('Expired 1 day ago', 'dividend');
										} elseif ( $diff->days != 0 ) {
											printf(__('Expired %d days ago', 'dividend'), $diff->days);
										} else {
											printf(__('Expired %d hours ago', 'dividend'), $diff->h);
										}
									} else {
										if ( $diff->days == 1 ) {
											_e('Ends in 1 day', 'dividend');
										} elseif ( $diff->days != 0 ) {
											printf(__('Ends in %d days', 'dividend'), $diff->days);
										} else {
											printf(__('Ends in %d hours', 'dividend'), $diff->h);
										}
									} ?>
								</div>
							</div>
						<?php endif; ?>

						<?php if ( isset($deals_no_expiry_date[0]) == 'on' ) : ?>
							<div class="deals-related-expiry"><?php _e('Ongoing', 'dividend' ); ?></div>
						<?php endif; ?>

						<h2 class="title front-view-title"><a href="<?php echo esc_url( get_the_permalink() ); ?>" title="<?php echo esc_attr( get_the_title() ); ?>"><?php the_title(); ?></a></h2>				
						<?php if( !empty( $deals_featured_text ) ) : ?>
							<div class="deals-featured">
								<?php echo $deals_featured_text; ?>
							</div>	
						<?php endif; ?>
						
						<div class="posts-more-button">
							<a href="<?php echo esc_url( get_the_permalink() ); ?>" title="<?php echo esc_attr( get_the_title() ); ?>" class="deals-more-button"><?php _e('More Info','dividend'); ?></a>
							<?php if( !empty($deals_button) ) { ?>
								<?php if ( !empty($deals_button_url) ) { ?><a href="<?php echo $deals_button_url; ?>" class="deals-button"> <?php } ?><?php echo $deals_button; ?><svg baseProfile="tiny" height="40px" version="1.2" viewBox="0 0 24 24" width="40px" fill="#ffffff" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Layer_1"><path d="M13.293,7.293c-0.391,0.391-0.391,1.023,0,1.414L15.586,11H8c-0.552,0-1,0.448-1,1s0.448,1,1,1h7.586l-2.293,2.293   c-0.391,0.391-0.391,1.023,0,1.414C13.488,16.902,13.744,17,14,17s0.512-0.098,0.707-0.293L19.414,12l-4.707-4.707   C14.316,6.902,13.684,6.902,13.293,7.293z"/></g></svg>
								<?php if ( !empty($deals_button_url) ) { ?></a><?php } ?> 	
							<?php } ?>
						</div>			    
					</div>
				</div>	
			</article><!--.post.excerpt-->
			<?php $j++; endwhile; endif; ?>
			<?php if ( $j !== 0 ) { // No pagination if there is no results
				mts_pagination('', 3, 'mts_deals_pagenavigation_type');
			} ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>