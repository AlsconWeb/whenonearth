<?php get_header();
$mts_options = get_option(MTS_THEME_NAME); ?>

<div id="page" class="<?php mts_single_page_class(); ?>">
	<div class="page-inner single-deals">	
		<article class="<?php mts_article_class(); ?>">
			<div id="content_box" >
			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>	
				<div id="post-<?php the_ID(); ?>">					
					<?php 	
					$deals_button = get_post_meta( get_the_ID(), 'mts_deals_button', true );
                    $deals_button_url = get_post_meta( get_the_ID(), 'mts_deals_button_url', true ); 
                    $deals_expiry_date = get_post_meta( get_the_ID(), 'mts_deals_expire' ); 
                    $deals_no_expiry_date = get_post_meta( get_the_ID(), 'mts_deals_no_expire_date'); 
                    // Single post parts ordering
					if ( isset( $mts_options['mts_deals_single_post_layout'] ) && is_array( $mts_options['mts_deals_single_post_layout'] ) && array_key_exists( 'enabled', $mts_options['mts_deals_single_post_layout'] ) ) {
						$single_post_parts = $mts_options['mts_deals_single_post_layout']['enabled'];
					} else {
						$single_post_parts = array( 'deals-content' => 'deals-content', 'deals-related' => 'deals-related', 'deals-subscribe' => 'deals-subscribe' );
					} 
					foreach( $single_post_parts as $part => $label ) {
						switch ($part) {
							case 'deals-content':
							?>
			                <div class="single_post">
			                    <header>
									<div class="single-post-category">
										<?php $term_list = wp_get_post_terms($post->ID, 'mts_categories', array("fields" => "all"));
					                    foreach ($term_list as $term) { ?>
					                        <a href="<?php echo get_term_link($term->term_id); ?>"><?php echo $term->slug; ?></a>			
					                    <?php } ?>
				                    </div>
									<h1 class="title entry-title"><?php the_title(); ?></h1>
								</header><!--.headline_area-->
		                    	<div class="single-deals-content">
									<div class="thecontent">
										<?php if ( has_post_thumbnail() && $mts_options['mts_single_deals_image'] == 1 ) : ?>
											<div class="single-thumbnail">
												<?php the_post_thumbnail('dividend-single',array('title' => '')); ?>
											</div>
										<?php endif; ?>	
										<?php the_content(); ?>
										<?php if( !empty($deals_button) ) { ?>
											<div class="single-deals-button">
												<?php if ( !empty($deals_button_url) ) { ?><a href="<?php echo $deals_button_url; ?>"> <?php } ?><?php echo $deals_button; ?><svg baseProfile="tiny" height="40px" version="1.2" viewBox="0 0 24 24" width="40px" fill="#ffffff" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Layer_1"><path d="M13.293,7.293c-0.391,0.391-0.391,1.023,0,1.414L15.586,11H8c-0.552,0-1,0.448-1,1s0.448,1,1,1h7.586l-2.293,2.293   c-0.391,0.391-0.391,1.023,0,1.414C13.488,16.902,13.744,17,14,17s0.512-0.098,0.707-0.293L19.414,12l-4.707-4.707   C14.316,6.902,13.684,6.902,13.293,7.293z"/></g></svg>
												<?php if ( !empty($deals_button_url) ) { ?></a><?php } ?> 	
											</div>
										<?php } ?>
										<?php if( !empty($deals_expiry_date) && empty($deals_no_expiry_date)) : ?>
											<div class="deals-expiry">
												<div class="expirydate">
													<?php
													$now = new DateTime(current_time('mysql'));
													$ref = new DateTime($deals_expiry_date[0]);
													$diff = $now->diff($ref);

													if ( $diff->invert ) {
														if ( $diff->days == 1 ) {
															_e('<span>Deal Expired: </span>Expired 1 day ago', 'dividend');
														} elseif ( $diff->days != 0 ) {
															printf(__('<span>Deal Expired: </span>Expired %d days ago', 'dividend'), $diff->days);
														} else {
															printf(__('<span>Deal Expired: </span>Expired %d hours ago', 'dividend'), $diff->h);
														}
													} else {
														if ( $diff->days == 1 ) {
															_e('<span>Deal Expires: </span>Ends in 1 day', 'dividend');
														} elseif ( $diff->days != 0 ) {
															printf(__('<span>Deal Expires: </span>Ends in %d days', 'dividend'), $diff->days);
														} else {
															printf(__('<span>Deal Expires: </span>Ends in %d hours', 'dividend'), $diff->h);
														}
													} ?>
												</div>
											</div>
										<?php endif; ?>
										<?php if ( isset($deals_no_expiry_date[0]) == 'on' ) : ?>
											<div class="deals-expiry"><div class="expirydate"><?php _e('<span>Deal Expires: </span>Ongoing', 'dividend' ); ?></div></div>
										<?php endif; ?>
										<div class="post-info">
									   		<?php if( isset($mts_options['mts_deals_single_meta_info_enable']['author-image']) == '1' ) { ?>
												 <div class="theauthorimage"><span><?php echo get_avatar( get_the_author_meta('email'), 75 ); ?></span></div>
											<?php } ?>
											<div class="single-meta-info">
												<?php if( isset($mts_options['mts_deals_single_meta_info_enable']['author']) == '1' ) { ?>
													<span class="theauthor"><span class="thespace"><?php _e('Found by','dividend'); ?>&nbsp;</span><span><?php the_author_posts_link(); ?></span></span>
												<?php }
												if( isset($mts_options['mts_deals_single_meta_info_enable']['category']) == '1' ) { ?>
													<span class="thecategory"><span class="thespace"><?php _e('in','dividend'); ?>&nbsp;</span><span><?php $term_list = wp_get_post_terms($post->ID, 'mts_categories', array("fields" => "all"));
					                    			foreach ($term_list as $term) { ?>
					                        			<a href="<?php echo get_term_link($term->term_id); ?>"><?php echo $term->slug; ?></a>			
					                    			<?php } ?></span></span>
												<?php }
												if( isset($mts_options['mts_deals_single_meta_info_enable']['comment']) == '1' ) { ?>
													<span class="thecomment"><a href="<?php echo esc_url( get_comments_link() ); ?>" itemprop="interactionCount"><i class="fa fa-comment"></i> <?php comments_number( '0', '1', '%' );?></a></span>
												<?php }
												if( isset($mts_options['mts_deals_single_meta_info_enable']['time']) == '1' ) { ?>
													<div class="thetime date updated"><span class="thespace"><?php _e('Updated','dividend'); ?>&nbsp;</span><span><?php the_time( get_option( 'date_format' ) ); ?></span></div>
												<?php } ?>
											</div>	
											<?php if (isset($mts_options['mts_social_button_position'])) mts_social_buttons(); ?>
										</div>
									</div>		
								</div>			
							</div><!--.single_post-->
							<?php
							break;

							case 'deals-related':
								mts_deals_related_posts();
							break;

							case 'deals-subscribe':
								if ( is_active_sidebar( 'widget-single-subscribe' ) ) { ?>
								    <div class="widget-single-subscribe">
									    <?php dynamic_sidebar( 'widget-single-subscribe' ); ?>
									</div>
								<?php }
							break;
							}
						}
					?>

				</div><!--.g post-->
				<?php comments_template( '', true ); ?>
				<?php endwhile; /* end loop */ ?>				
			</div>
		</article>
		<?php get_sidebar(); ?>
	</div>	
<?php get_footer(); ?>