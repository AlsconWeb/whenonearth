<?php 

add_action( 'wp_enqueue_scripts', 'salient_child_enqueue_styles');
function salient_child_enqueue_styles() {
	
		$nectar_theme_version = nectar_get_theme_version();
		
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css', array('font-awesome'), $nectar_theme_version);
    wp_enqueue_script( 'lazy', get_template_directory_uri() . '/js/jquery.lazy.min.js', array('jquery'), $nectar_theme_version);

    if ( is_rtl() )  
   		wp_enqueue_style(  'salient-rtl',  get_template_directory_uri(). '/rtl.css', array(), '1', 'screen' );
}
// require_once 'blog.php';
require_once 'shortcodes.php';

add_action( 'after_setup_theme', 'alscon_add_image_sizes' );

function alscon_add_image_sizes(){
  add_image_size( 'thumbnail_post_square', 412, 482, true );
  add_image_size( 'thumbnail_post_vertical', 287, 370, true );
  add_image_size( 'thumbnail_post_vertical_big', 459, 643, true );
  add_image_size( 'thumbnail_post_square_mini', 80, 53, true );
  add_image_size( 'thumbnail_post_square_in_post', 412, 257, true);
  add_image_size( 'thumbnail_post_square_middle', 298, 191, true);
}

add_action( 'add_meta_boxes', 'add_meta_box_sidebar' );

function add_meta_box_sidebar(){
  add_meta_box( 'sidebars_choice', 'Choice Sidebar', 'choice_sidebar', 'post', 'side', 'core');
}

function choice_sidebar($post, $meta){
  $screens = $meta['args'];
  
  wp_nonce_field('choice_sidebar', 'choice_sidebar');

  $sidebar_choice = get_post_meta( $post->ID, 'sidebar_choice', true );
  
  echo '<label for="select_sidebar">' . __("Description for this field", 'myplugin_textdomain' ) . '</label> ';
  echo '<select  id= "select_sidebar"  name="sidebar_choice">';

  if($sidebar_choice == 'no'){
    echo '<option value="no" selected>No Sidebar</option>';
  }
  else{
    echo '<option value="no">No Sidebar</option>';
  }
  if($sidebar_choice == 'def'){
    echo '<option value="def" selected>Default Sidebar</option>';
  }else{
    echo '<option value="def">Default Sidebar</option>';
  }
  
  echo '</select>';
}

add_action( 'save_post', 'save_post_data' );
function save_post_data ( $post_id ){
  if ( ! isset( $_POST['sidebar_choice'] ) )
		return;
	update_post_meta( $post_id, 'sidebar_choice', $_POST['sidebar_choice']);
}
?>