<?php
/**
 * Shortcodes
 *
 * @package Salient WordPress Theme
 * @subpackage helpers
 * @version 9.0.2
 */


// Shortcode Processing
if ( ! function_exists( 'nectar_shortcode_processing' ) ) {
	function nectar_shortcode_processing() {
		require_once 'shortcode-processing.php';
	}
}


add_action( 'init', 'nectar_shortcode_processing' );