<?php
/**
 * The template for displaying the header.
 *
 * Displays everything from the doctype declaration down to the navigation.
 */
?>
<!DOCTYPE html>
<?php $mts_options = get_option(MTS_THEME_NAME); ?>
<html class="no-js" <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
	<!--[if IE ]>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<![endif]-->
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<?php mts_meta(); ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<?php wp_head(); ?>
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-133876848-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-133876848-1');
</script>
</head>
<body id="blog" <?php body_class('main'); ?>>
	<div class="main-container">
		<?php if( $mts_options['mts_sticky_nav'] == '1' ) { ?>
			<div id="catcher" class="clear" ></div>
			<header id="site-header" class="sticky-navigation" role="banner" itemscope itemtype="http://schema.org/WPHeader">
		<?php } else { ?>
			<header id="site-header" role="banner" itemscope itemtype="http://schema.org/WPHeader">
		<?php } ?>
			<div id="header" class="container clearfix">
				<div class="logo-wrap">
					<?php if ( $mts_options['mts_logo'] != '' && $mts_logo = wp_get_attachment_image_src( $mts_options['mts_logo'], 'full' ) ) { ?>
						<?php if ( is_front_page() || is_home() || is_404() ) { ?>
							<h1 id="logo" class="image-logo" itemprop="headline">
								<a href="<?php echo esc_url( home_url() ); ?>"><img src="<?php echo esc_url( $mts_logo[0] ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" width="<?php echo esc_attr( $mts_logo[1] ); ?>" height="<?php echo esc_attr( $mts_logo[2] ); ?>"></a>
							</h1><!-- END #logo -->
						<?php } else { ?>
							<h2 id="logo" class="image-logo" itemprop="headline">
								<a href="<?php echo esc_url( home_url() ); ?>">
									<img src="<?php echo esc_url( $mts_logo[0] ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" width="<?php echo esc_attr( $mts_logo[1] ); ?>" height="<?php echo esc_attr( $mts_logo[2] ); ?>"></a>
							</h2><!-- END #logo -->
						<?php } ?>

					<?php } else { ?>

						<?php if ( is_front_page() || is_home() || is_404() ) { ?>
							<h1 id="logo" class="text-logo" itemprop="headline">
								<a href="<?php echo esc_url( home_url() ); ?>"><?php bloginfo( 'name' ); ?></a>
							</h1><!-- END #logo -->
						<?php } else { ?>
							<h2 id="logo" class="text-logo" itemprop="headline">
								<a href="<?php echo esc_url( home_url() ); ?>"><?php bloginfo( 'name' ); ?></a>
							</h2><!-- END #logo -->
						<?php } ?>
					<?php } ?>
				</div>
				<?php if ( $mts_options['mts_show_primary_nav'] == '1' ) { ?>
					<div id="secondary-navigation" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
						<a href="#" id="pull" class="toggle-mobile-menu"><?php _e('Menu', 'dividend' ); ?></a>
						<?php if ( has_nav_menu( 'mobile' ) ) { ?>
							<nav class="navigation clearfix">
								<?php if ( has_nav_menu( 'primary-menu' ) ) { ?>
									<?php wp_nav_menu( array( 'theme_location' => 'primary-menu', 'menu_class' => 'menu clearfix', 'container' => '', 'walker' => new mts_menu_walker ) ); ?>
								<?php } else { ?>
									<ul class="menu clearfix">
										<?php wp_list_categories('title_li='); ?>
									</ul>
								<?php } ?>
							</nav>
							<nav class="navigation mobile-only clearfix mobile-menu-wrapper">
								<?php wp_nav_menu( array( 'theme_location' => 'mobile', 'menu_class' => 'menu clearfix', 'container' => '', 'walker' => new mts_menu_walker ) ); ?>
							</nav>
						<?php } else { ?>
							<nav class="navigation clearfix mobile-menu-wrapper">
								<?php if ( has_nav_menu( 'primary-menu' ) ) { ?>
									<?php wp_nav_menu( array( 'theme_location' => 'primary-menu', 'menu_class' => 'menu clearfix', 'container' => '', 'walker' => new mts_menu_walker ) ); ?>
								<?php } else { ?>
									<ul class="menu clearfix">
										<?php wp_list_categories('title_li='); ?>
									</ul>
								<?php } ?>
							</nav>
						<?php } ?>
					</div>
				<?php } ?>
				<?php if ( !empty($mts_options['mts_header_search']) ) { ?>
					<div id="search-6" class="widget widget_search">
						<?php get_search_form(); ?>
					</div><!-- END #search-6 -->
				<?php } ?>
				<?php mts_header_buttons(); ?>
				<?php if( $mts_options['mts_show_login'] == '1' ) { ?>
					<div class="header-login">
						<?php if ( is_user_logged_in() ) { ?>
							<div class="login-wrap logged-in">
								<?php $current_user = wp_get_current_user();
								echo get_avatar( $current_user->ID, 30 ); ?>
								<span class="user-name"><?php echo $current_user->display_name; ?></span>
								<div class="logged-in-sub">
									<a href="<?php echo get_edit_user_link(); ?>"><?php _e('Edit Profile','dividend'); ?></a>
									<a href="<?php echo wp_logout_url(); ?>"><?php _e('Logout','dividend'); ?></a>
								</div>
							</div>
						<?php } else { ?>
							<div class="login-signup not-logged-in">
								<a href="<?php echo wp_login_url(); ?>"><?php _e('<i class="fa fa-user"></i> Join','dividend'); ?></a>
								<a href="<?php echo wp_registration_url(); ?>"><?php _e('<i class="fa fa-lock"></i> Sign up','dividend'); ?></a>
							</div>
						<?php } ?>
					</div>
				<?php } ?>
			</div><!--#header-->
		</header>
		<?php if ( is_active_sidebar( 'widget-header' ) ) { ?>
			<div class="container">
				<?php dynamic_sidebar('widget-header'); ?>
			</div>
		<?php } ?>
