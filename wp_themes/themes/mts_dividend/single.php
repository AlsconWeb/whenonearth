<?php
/**
 * The template for displaying all single posts.
 */
$mts_options = get_option(MTS_THEME_NAME);

get_header(); ?>

<div id="page" class="<?php mts_single_page_class(); ?>">
	<div class="page-inner">	
		<article class="<?php mts_article_class(); ?>">
			<div id="content_box" >
				<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
					<div id="post-<?php the_ID(); ?>" <?php post_class('g post'); ?>>
						<?php if ($mts_options['mts_breadcrumb'] == '1') {
							if( function_exists( 'rank_math' ) && rank_math()->breadcrumbs ) {
							    rank_math_the_breadcrumbs();
							  } else { ?>
							    <div class="breadcrumb" xmlns:v="http://rdf.data-vocabulary.org/#"><?php mts_the_breadcrumb(); ?></div>
							<?php }
						}	  
						// Single post parts ordering
						if ( isset( $mts_options['mts_single_post_layout'] ) && is_array( $mts_options['mts_single_post_layout'] ) && array_key_exists( 'enabled', $mts_options['mts_single_post_layout'] ) ) {
							$single_post_parts = $mts_options['mts_single_post_layout']['enabled'];
						} else {
							$single_post_parts = array( 'content' => 'content', 'subscribe-box' => 'subscribe-box', 'related' => 'related', 'author' => 'author' );
						}
						foreach( $single_post_parts as $part => $label ) { 
							switch ($part) {
								case 'content':
									?>
									<div class="single_post">
										<?php $header_animation = mts_get_post_header_effect(); ?>
										<?php if ( 'parallax' === $header_animation ) {?>
											<?php if (mts_get_thumbnail_url()) : ?>
												<div id="parallax" <?php echo 'style="background-image: url('.mts_get_thumbnail_url().');"'; ?>></div>
											<?php endif; ?>
										<?php } else if ( 'zoomout' === $header_animation ) {?>
											 <?php if (mts_get_thumbnail_url()) : ?>
												<div id="zoom-out-effect"><div id="zoom-out-bg" <?php echo 'style="background-image: url('.mts_get_thumbnail_url().');"'; ?>></div></div>
											<?php endif; ?>
										<?php } ?>
										<header>
											<?php if ( $mts_options['mts_single_post_category'] == '1' ) { ?>
											<div class="single-post-category">
												<?php $category = get_the_category();
												    $name = $category[0]->cat_name;
											        $cat_id = get_cat_ID( $name );
											        $link = get_category_link( $cat_id );
											        echo '<a href="'. esc_url( $link ) .'"">'. $name .'</a>'; ?>
										    </div>
										    <?php } ?>
											<h1 class="title single-title entry-title"><?php the_title(); ?></h1>
											<div class="post-info">
										   		<?php if( isset($mts_options['mts_single_meta_info_enable']['author-image']) == '1' ) { ?>
													 <div class="theauthorimage"><span><?php echo get_avatar( get_the_author_meta('email'), 75 ); ?></span></div>
												<?php } ?>
												<div class="single-meta-info">
													<?php if( isset($mts_options['mts_single_meta_info_enable']['author']) == '1' ) { ?>
														<span class="theauthor"><span class="thespace"><?php _e('by','dividend'); ?>&nbsp;</span><span><?php the_author_posts_link(); ?></span></span>
													<?php }
													if( isset($mts_options['mts_single_meta_info_enable']['category']) == '1' ) { ?>
														<span class="thecategory"><span class="thespace"><?php _e('in','dividend'); ?>&nbsp;</span><span><?php mts_the_category(', ') ?></span></span>
													<?php }
													if( isset($mts_options['mts_single_meta_info_enable']['comment']) == '1' ) { ?>
														<span class="thecomment"><a href="<?php echo esc_url( get_comments_link() ); ?>" itemprop="interactionCount"><i class="fa fa-comment"></i> <?php comments_number( '0', '1', '%' );?></a></span>
													<?php }
													if( isset($mts_options['mts_single_meta_info_enable']['time']) == '1' ) { ?>
														<div class="thetime date updated"><span class="thespace"><?php _e('Updated','dividend'); ?>&nbsp;</span><span><?php the_time( get_option( 'date_format' ) ); ?></span></div>
													<?php } ?>
												</div>	
												<?php if (isset($mts_options['mts_social_button_position']) && $mts_options['mts_social_button_position'] !== 'bottom') mts_social_buttons(); ?>
											</div>
										</header><!--.headline_area-->
										<?php if ( has_post_thumbnail() && ( 'parallax' !== $header_animation && 'zoomout' !== $header_animation ) && $mts_options['mts_single_featured_image'] == 1 ) : ?>
											<div class="featured-thumbnail">
												<?php the_post_thumbnail('dividend-bigthumb',array('title' => '')); ?>
											</div>
										<?php endif; ?>	
										<div class="post-single-content box mark-links entry-content">
											<?php // Top Ad Code ?>
											<?php if ($mts_options['mts_posttop_adcode'] != '') { ?>
												<?php $toptime = $mts_options['mts_posttop_adcode_time']; if (strcmp( date("Y-m-d", strtotime( "-$toptime day")), get_the_time("Y-m-d") ) >= 0) { ?>
													<div class="topad">
														<?php echo do_shortcode($mts_options['mts_posttop_adcode']); ?>
													</div>
												<?php } ?>
											<?php } ?>

											<?php // Content ?>
											<div class="thecontent">
												<?php the_content(); ?>
											</div>

											<?php // Single Pagination ?>
											<?php wp_link_pages(array('before' => '<div class="pagination">', 'after' => '</div>', 'link_before'  => '<span class="current"><span class="currenttext">', 'link_after' => '</span></span>', 'next_or_number' => 'next_and_number', 'nextpagelink' => __('Next', 'dividend' ), 'previouspagelink' => __('Previous', 'dividend' ), 'pagelink' => '%','echo' => 1 )); ?>

											<?php // Bottom Ad Code ?>
											<?php if ($mts_options['mts_postend_adcode'] != '') { ?>
												<?php $endtime = $mts_options['mts_postend_adcode_time']; if (strcmp( date("Y-m-d", strtotime( "-$endtime day")), get_the_time("Y-m-d") ) >= 0) { ?>
													<div class="bottomad">
														<?php echo do_shortcode($mts_options['mts_postend_adcode']); ?>
													</div>
												<?php } ?>
											<?php } ?>

											<?php // Bottom Social Share ?>
											<?php if (isset($mts_options['mts_social_button_position']) && $mts_options['mts_social_button_position'] == 'bottom') mts_social_buttons(); ?>
										</div><!--.post-single-content-->
									</div><!--.single_post-->
									<?php
								break;

								case 'subscribe-box':
									if ( is_active_sidebar( 'widget-single-subscribe' ) ) { ?>
										<div class="widget-single-subscribe">
											<?php dynamic_sidebar( 'widget-single-subscribe' ); ?>
										</div>	
									<?php }
								break;

								case 'tags':
									?>
									<?php mts_the_tags('<div class="tags">',' ') ?>
									<?php
								break;

								case 'related':
									mts_related_posts();
								break;

								case 'author':
									?>
									<div class="postauthor">
										<h4><?php _e('About The Author', 'dividend' ); ?></h4>
										<div class="postauthor-wrap">
											<?php if(function_exists('get_avatar')) { echo get_avatar( get_the_author_meta('email'), '100' );  } ?>
											<h5 class="vcard author"><a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" class="fn"><?php the_author_meta( 'display_name' ); ?></a></h5>
											<p><?php the_author_meta('description') ?></p>
										</div>
									</div>
									<?php
								break;
							}
						}
						?>
					</div><!--.g post-->
					<?php comments_template( '', true ); ?>
				<?php endwhile; /* end loop */ ?>
			</div>
		</article>
		<?php get_sidebar(); ?>
	</div>
<?php get_footer(); ?>
