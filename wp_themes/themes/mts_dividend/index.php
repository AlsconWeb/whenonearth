<?php
/**
 * The main template file.
 *
 * Used to display the homepage when home.php doesn't exist.
 */
$mts_options = get_option(MTS_THEME_NAME);

get_header();

if ( !is_paged() && $mts_options['mts_full_featured_slider'] == '1' ) { //Featured Slider Section
	get_template_part('home/section', 'featured-slider' );
}

if ( !is_paged() && is_home() && $mts_options['mts_featured_carousel'] == '1' ) { //Featured Carousel Section
	get_template_part('home/section', 'featured-carousel' );
}

if ( !is_paged() && is_home() && $mts_options['mts_deals_carousel'] == '1' ) { //Homepage Deals Section
	get_template_part('home/section', 'homepage-deals' );
} ?>

<div id="page" class="clearfix">
	<div class="article">
		<div id="content_box">
			<?php if ( !is_paged() ) { ?>

				<?php $sidebar_used = false;
				$sidebar_used6 = false;
				$featured_categories = array();
				if ( !empty( $mts_options['mts_featured_categories'] ) ) { //Featured Categories Section
					foreach ( $mts_options['mts_featured_categories'] as $section ) {
						$category_id = $section['mts_featured_category'];
						$featured_categories[] = $category_id;
						$posts_num = $section['mts_featured_category_postsnum'];
						$post_color = $section['mts_featured_category_color'];
						if( !empty( $section['mts_home_meta_info_enable'] ) ) {
							$post_author = $section['mts_home_meta_info_enable']['author'];
							$post_category = $section['mts_home_meta_info_enable']['category'];
							$post_time = $section['mts_home_meta_info_enable']['time'];
							$post_comment = $section['mts_home_meta_info_enable']['comment'];
						}
						$layout = isset( $section['mts_featured_category_layout'] ) ? $section['mts_featured_category_layout'] : 'layout-2';
						if ( 'latest' == $category_id ) { ?>
							<div class="article-wrap">
								<div class="container">
									<div class="article-inner">
										<h3 class="featured-category-title" style="background: <?php echo $post_color; ?>;"><?php _e('Latest Posts', 'dividend' ); ?></h3>
										<?php switch ($layout) {
											case 'layout-1':
												echo '<div class="article-layout-1">'; //Layout 1 having Four Grid Post
												break;

											case 'layout-2':
												$sidebar_used = true;
												echo '<div class="article-layout-2">'; //Layout 2 having 1 Big Post With Sidebar
												echo '<div class="article">';
												break;

											case 'layout-3':
												echo '<div class="article-layout-3">'; //Layout 3 having 1 Big Post
												break;

											case 'layout-4':
												echo '<div class="article-layout-4">'; //Layout 4 having 2 Big Post
												break;

											case 'layout-5':
												echo '<div class="article-layout-5">'; //Layout 5 having 3 Big Post
												break;

											case 'layout-6':
												$sidebar_used6 = true;
												echo '<div class="article-layout-6">'; //Layout 6 having Small Post With Sidebar
												echo '<div class="article">';
												break;	

											case 'layout-7':
												echo '<div class="article-layout-7">'; //Layout 7 having 6 Small Post in a row
												break;
											
											default:
												$sidebar_used = true;
												echo '<div class="article-layout-2">';
												echo '<div class="article">';
												break;
										}


										$j = 1; if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
											<?php get_template_part('home/section', $layout ); ?>
										<?php ++$j; endwhile; endif;

										if ( $layout == 'layout-1' || $layout == 'layout-2' || $layout == 'layout-3' || $layout == 'layout-4' || $layout == 'layout-5' || $layout == 'layout-6' || $layout == 'layout-7' ) {
											if ( $j !== 0 ) { // No pagination if there is no posts
												mts_pagination();
											} ?> 
											</div>
										<?php }

										if ( $layout == 'layout-2' ) {
											$sidebar_used = true;
											$cat_name = __( 'Home Layout 2', 'dividend' );									
											$sidebar_name = sanitize_title( strtolower( 'post-layout-2'.$cat_name ));
		                    				get_category_sidebar($sidebar_name, $cat_name); ?>
		                    				</div> 
										<?php }						

										if ( $layout == 'layout-6' ) {		
											$sidebar_used6 = true;							
											$cat_name = __( 'Home Layout 6', 'dividend' );							
											$sidebar_name = sanitize_title( strtolower( 'post-layout-6'.$cat_name ));
		                    				get_category_sidebar($sidebar_name, $cat_name); ?>
		                    				</div> 
										<?php } ?>		
									</div>	<!-- article inner -->				
								</div>	<!-- article-wrap-layouts -->
							</div> <!-- article-wrap -->
							
						<?php } else { // if $category_id != 'latest': ?>
							<div class="article-wrap">
								<div class="container">
									<div class="article-inner">
										<h3 class="featured-category-title" style="background: <?php echo $post_color; ?>;"><a href="<?php echo esc_url( get_category_link( $category_id ) ); ?>" title="<?php echo esc_attr( get_cat_name( $category_id ) ); ?>"><?php echo esc_html( get_cat_name( $category_id ) ); ?></a></h3>
										<a href="<?php echo esc_url( get_category_link( $category_id ) ); ?>" title="<?php echo esc_attr( get_cat_name( $category_id ) ); ?>" class="btn-archive-link"><?php _e( 'View all', 'dividend' ); ?></a>

										<?php switch ($layout) {
											case 'layout-1':
												echo '<div class="article-layout-1">'; //Layout 1 having Four Grid Post
												break;

											case 'layout-2':
												$sidebar_used = true;
												echo '<div class="article-layout-2">'; //Layout 2 having 1 Big Post With Sidebar
												echo '<div class="article">';
												break;

											case 'layout-3':
												echo '<div class="article-layout-3">'; //Layout 3 having 1 Big Post
												break;

											case 'layout-4':
												echo '<div class="article-layout-4">'; //Layout 4 having 2 Big Post
												break;

											case 'layout-5':
												echo '<div class="article-layout-5">'; //Layout 5 having 3 Big Post
												break;

											case 'layout-6':
												$sidebar_used6 = true;
												echo '<div class="article-layout-6">'; //Layout 6 having Small Post With Sidebar
												echo '<div class="article">';
												break;	

											case 'layout-7':
												echo '<div class="article-layout-7">'; //Layout 7 having 6 Small Post in a row
												break;
											
											default:
												$sidebar_used = true;
												echo '<div class="article-layout-2">';
												echo '<div class="article">';
												break;
										}

										$j = 1; $cat_query = new WP_Query('cat='.$category_id.'&posts_per_page='.$posts_num);
										if ( $cat_query->have_posts() ) : while ( $cat_query->have_posts() ) : $cat_query->the_post(); ?>
											<?php get_template_part('home/section', $layout ); ?>
										<?php ++$j; endwhile; endif; wp_reset_postdata();

										if ( $layout == 'layout-1' || $layout == 'layout-2' || $layout == 'layout-3' || $layout == 'layout-4' || $layout == 'layout-5' || $layout == 'layout-6' || $layout == 'layout-7' ) { ?>
											</div>
										<?php }

										if ( $layout == 'layout-2' ) {
											$sidebar_used = true;
											$cat_name = ucwords(get_cat_name( $category_id ));									
											$sidebar_name = sanitize_title( strtolower( 'post-layout-2'.$cat_name ));
		                    				get_category_sidebar($sidebar_name, $cat_name); ?>
		                    				</div> 
										<?php }						

										if ( $layout == 'layout-6' ) {									
											$sidebar_used6 = true;
											$cat_name = ucwords(get_cat_name( $category_id ));										
											$sidebar_name = sanitize_title( strtolower( 'post-layout-6'.$cat_name ));
		                    				get_category_sidebar($sidebar_name, $cat_name); ?>
		                    				</div> 
										<?php } ?>		
									</div>	<!-- article inner --> 				
								</div> <!-- article-wrap-layouts -->
							</div> <!-- article-wrap -->
						<?php }
					}
				} ?>

			<?php } else { //Paged ?>

				<?php foreach ( $mts_options['mts_featured_categories'] as $section ) {
					$category_id = $section['mts_featured_category'];
					$featured_categories[] = $category_id;
					$posts_num = $section['mts_featured_category_postsnum'];
					$post_color = $section['mts_featured_category_color'];
					if( !empty( $section['mts_home_meta_info_enable'] ) ) {
						$post_author = $section['mts_home_meta_info_enable']['author'];
						$post_category = $section['mts_home_meta_info_enable']['category'];
						$post_time = $section['mts_home_meta_info_enable']['time'];
						$post_comment = $section['mts_home_meta_info_enable']['comment'];
					}
					$layout = isset( $section['mts_featured_category_layout'] ) ? $section['mts_featured_category_layout'] : 'layout-2';
					if ( 'latest' == $category_id ) { ?>
					<div class="article-wrap">
						<div class="container">
							<div class="article-inner">
								
								<?php switch ($layout) {
									case 'layout-1':
										echo '<div class="article-layout-1">'; //Layout 1 having Four Grid Post
										break;

									case 'layout-2':
										$sidebar_used = true;
										echo '<div class="article-layout-2">'; //Layout 2 having 1 Big Post With Sidebar
										echo '<div class="article">';
										break;

									case 'layout-3':
										echo '<div class="article-layout-3">'; //Layout 3 having 1 Big Post
										break;

									case 'layout-4':
										echo '<div class="article-layout-4">'; //Layout 4 having 2 Big Post
										break;

									case 'layout-5':
										echo '<div class="article-layout-5">'; //Layout 5 having 3 Big Post
										break;

									case 'layout-6':
										$sidebar_used6 = true;
										echo '<div class="article-layout-6">'; //Layout 6 having Small Post With Sidebar
										echo '<div class="article">';
										break;	

									case 'layout-7':
										echo '<div class="article-layout-7">'; //Layout 7 having 6 Small Post in a row
										break;
									
									default:
										$sidebar_used = true;
										echo '<div class="article-layout-2">';
										echo '<div class="article">';
										break;
								}

								$j = 1; if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
									<?php get_template_part('home/section', $layout ); ?>
								<?php ++$j; endwhile; endif;

								if ( $layout == 'layout-1' || $layout == 'layout-2' || $layout == 'layout-3' || $layout == 'layout-4' || $layout == 'layout-5' || $layout == 'layout-6' || $layout == 'layout-7' ) {
									if ( $j !== 0 ) { // No pagination if there is no posts
										mts_pagination();
									} ?> 
									</div>
								<?php }

								if ( $layout == 'layout-2' ) {
									$sidebar_used = true;
									$cat_name = __( 'Home Layout 2', 'dividend' );									
									$sidebar_name = sanitize_title( strtolower( 'post-layout-2'.$cat_name ));
                    				get_category_sidebar($sidebar_name, $cat_name); ?>
                    				</div> 
								<?php }						

								if ( $layout == 'layout-6' ) {		
									$sidebar_used6 = true;							
									$cat_name = __( 'Home Layout 6', 'dividend' );							
									$sidebar_name = sanitize_title( strtolower( 'post-layout-6'.$cat_name ));
                    				get_category_sidebar($sidebar_name, $cat_name); ?>
                    				</div> 
								<?php } ?>		
							</div>	<!-- article inner -->				
						</div>	<!-- article-wrap-layouts -->
					</div> <!-- article-wrap -->
				<?php }
				}
			} ?>
		</div>
	</div>

	<?php if ( !is_paged() && $mts_options['mts_call_to_action'] == '1') { // Call to Action Section
		get_template_part('home/section', 'call-to-action' );
	} ?>

<?php get_footer(); ?>