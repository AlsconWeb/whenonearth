<?php
$mts_options = get_option(MTS_THEME_NAME);
if ( ! function_exists( 'mts_meta' ) ) {
	/**
	 * Display necessary tags in the <head> section.
	 */
	function mts_meta(){
		global $mts_options, $post;
		?>

<?php if ( ! empty( $mts_options['mts_favicon'] ) && $mts_favicon = wp_get_attachment_url( $mts_options['mts_favicon'] ) ) { ?>
<link rel="icon" href="<?php echo esc_url( $mts_favicon ); ?>" type="image/x-icon" />
<?php } elseif ( function_exists( 'has_site_icon' ) && has_site_icon() ) { ?>
<?php printf( '<link rel="icon" href="%s" sizes="32x32" />', esc_url( get_site_icon_url( 32 ) ) ); ?>
<?php sprintf( '<link rel="icon" href="%s" sizes="192x192" />', esc_url( get_site_icon_url( 192 ) ) ); ?>
<?php } ?>

<?php if ( !empty( $mts_options['mts_metro_icon'] ) && $mts_metro_icon = wp_get_attachment_url( $mts_options['mts_metro_icon'] ) ) { ?>
<!-- IE10 Tile.-->
<meta name="msapplication-TileColor" content="#FFFFFF">
<meta name="msapplication-TileImage" content="<?php echo esc_url( $mts_metro_icon ); ?>">
<?php } elseif ( function_exists( 'has_site_icon' ) && has_site_icon( ) ) { ?>
<?php printf( '<meta name="msapplication-TileImage" content="%s">', esc_url( get_site_icon_url( 270 ) ) ); ?>
<?php } ?>

<?php if ( ! empty( $mts_options['mts_touch_icon'] ) && $mts_touch_icon = wp_get_attachment_url( $mts_options['mts_touch_icon'] ) ) { ?>
<!--iOS/android/handheld specific -->
<link rel="apple-touch-icon-precomposed" href="<?php echo esc_url( $mts_touch_icon ); ?>" />
<?php } elseif ( function_exists( 'has_site_icon' ) && has_site_icon() ) { ?>
<?php printf( '<link rel="apple-touch-icon-precomposed" href="%s">', esc_url( get_site_icon_url( 180 ) ) ); ?>
<?php } ?>

<?php if ( ! empty( $mts_options['mts_responsive'] ) ) { ?>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<?php } ?>

<?php if($mts_options['mts_prefetching'] == '1') { ?>
<?php if (is_front_page()) { ?>
<?php $my_query = new WP_Query('posts_per_page=1'); while ($my_query->have_posts()) : $my_query->the_post(); ?>
<link rel="prefetch" href="<?php the_permalink(); ?>">
<link rel="prerender" href="<?php the_permalink(); ?>">
<?php endwhile; wp_reset_postdata(); ?>
<?php } elseif (is_singular()) { ?>
<link rel="prefetch" href="<?php echo esc_url( home_url() ); ?>">
<link rel="prerender" href="<?php echo esc_url( home_url() ); ?>">
<?php } ?>
<?php } ?>
<?php
	}
}

if ( ! function_exists( 'mts_head' ) ){
	/**
	 * Display header code from Theme Options.
	 */
	function mts_head() {
	global $mts_options;
?>
<?php echo $mts_options['mts_header_code']; ?>
<?php }
}
add_action('wp_head', 'mts_head');

if ( ! function_exists( 'mts_copyrights_credit' ) ) {
	/**
	 * Display the footer copyright.
	 */
	function mts_copyrights_credit() {
	global $mts_options;
?>
<!--start copyrights-->
<div class="row" id="copyright-note">
    <?php if ( $mts_options['mts_show_footer_nav'] == '1' ) { ?>
    <nav class="footer-navigation">
        <?php if ( has_nav_menu( 'footer-menu' ) ) { ?>
        <?php wp_nav_menu( array( 'theme_location' => 'footer-menu', 'menu_class' => 'menu clearfix', 'container' => '', 'walker' => new mts_menu_walker ) ); ?>
        <?php } ?>
    </nav>
    <?php } ?>
    <?php $copyright_text = '&copy;&nbsp;' . date("Y") . '&nbsp;<a href=" ' . esc_url( trailingslashit( home_url() ) ). '" title=" ' . get_bloginfo('description') . '">' . get_bloginfo('name') . '</a>. All Rights Reserved.'; ?>
    <span><?php echo apply_filters( 'mts_copyright_content', $copyright_text ); ?></span>
    <div class="to-top"><?php echo $mts_options['mts_copyrights']; ?></div>
</div>
<!--end copyrights-->
<?php }
}

if ( ! function_exists( 'mts_footer' ) ) {
	/**
	 * Display the analytics code in the footer.
	 */
	function mts_footer() {
	global $mts_options;
?>
<?php if ($mts_options['mts_analytics_code'] != '') { ?>
<!--start footer code-->
<?php echo $mts_options['mts_analytics_code']; ?>
<!--end footer code-->
<?php }
	}
}

if ( ! function_exists('mts_the_breadcrumb') ) {
	/**
	 * Display the breadcrumbs.
	 */
	function mts_the_breadcrumb() {
		if ( is_front_page() ) {
			return;
		}
		echo '<div><i class="fa fa-home"></i></div> <div typeof="v:Breadcrumb" class="root"><a rel="v:url" property="v:title" href="';
		echo esc_url( home_url() );
		echo '">'.esc_html__( "Home", 'dividend' );
		echo '</a></div><div><i class="fa fa-caret-right"></i></div>';
		if (is_single()) {
			$categories = get_the_category();
			if ( $categories ) {
				$level = 0;
				$hierarchy_arr = array();
				foreach ( $categories as $cat ) {
					$anc = get_ancestors( $cat->term_id, 'category' );
					$count_anc = count( $anc );
					if (  0 < $count_anc && $level < $count_anc ) {
						$level = $count_anc;
						$hierarchy_arr = array_reverse( $anc );
						array_push( $hierarchy_arr, $cat->term_id );
					}
				}
				if ( empty( $hierarchy_arr ) ) {
					$category = $categories[0];
					echo '<div typeof="v:Breadcrumb"><a href="'. esc_url( get_category_link( $category->term_id ) ).'" rel="v:url" property="v:title">'.esc_html( $category->name ).'</a></div><div><i class="fa fa-caret-right"></i></div>';
				} else {
					foreach ( $hierarchy_arr as $cat_id ) {
						$category = get_term_by( 'id', $cat_id, 'category' );
						echo '<div typeof="v:Breadcrumb"><a href="'. esc_url( get_category_link( $category->term_id ) ).'" rel="v:url" property="v:title">'.esc_html( $category->name ).'</a></div><div><i class="fa fa-caret-right"></i></div>';
					}
				}
			}
			echo "<div><span>";
			the_title();
			echo "</span></div>";
		} elseif (is_page()) {
			$parent_id  = wp_get_post_parent_id( get_the_ID() );
			if ( $parent_id ) {
				$breadcrumbs = array();
				while ( $parent_id ) {
					$page = get_page( $parent_id );
					$breadcrumbs[] = '<div typeof="v:Breadcrumb"><a href="'.esc_url( get_permalink( $page->ID ) ).'" rel="v:url" property="v:title">'.esc_html( get_the_title($page->ID) ). '</a></div><div><i class="fa fa-caret-right"></i></div>';
					$parent_id  = $page->post_parent;
				}
				$breadcrumbs = array_reverse( $breadcrumbs );
				foreach ( $breadcrumbs as $crumb ) { echo $crumb; }
			}
			echo "<div><span>";
			the_title();
			echo "</span></div>";
		} elseif (is_category()) {
			global $wp_query;
			$cat_obj = $wp_query->get_queried_object();
			$this_cat_id = $cat_obj->term_id;
			$hierarchy_arr = get_ancestors( $this_cat_id, 'category' );
			if ( $hierarchy_arr ) {
				$hierarchy_arr = array_reverse( $hierarchy_arr );
				foreach ( $hierarchy_arr as $cat_id ) {
					$category = get_term_by( 'id', $cat_id, 'category' );
					echo '<div typeof="v:Breadcrumb"><a href="'.esc_url( get_category_link( $category->term_id ) ).'" rel="v:url" property="v:title">'.esc_html( $category->name ).'</a></div><div><i class="fa fa-caret-right"></i></div>';
				}
			}
			echo "<div><span>";
			single_cat_title();
			echo "</span></div>";
		} elseif (is_author()) {
			echo "<div><span>";
			if(get_query_var('author_name')) :
				$curauth = get_user_by('slug', get_query_var('author_name'));
			else :
				$curauth = get_userdata(get_query_var('author'));
			endif;
			echo esc_html( $curauth->nickname );
			echo "</span></div>";
		} elseif (is_search()) {
			echo "<div><span>";
			the_search_query();
			echo "</span></div>";
		} elseif (is_tag()) {
			echo "<div><span>";
			single_tag_title();
			echo "</span></div>";
		}
	}
}
if ( ! function_exists( 'mts_the_category' ) ) {
/**
 * Display schema-compliant the_category()
 *
 * @param string $separator
 */
	function mts_the_category( $separator = ', ' ) {
		$categories = get_the_category();
		$count = count($categories);
		foreach ( $categories as $i => $category ) {
			echo '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" title="' . sprintf( __( "View all posts in %s", 'dividend' ), esc_attr( $category->name ) ) . '">' . esc_html( $category->name ).'</a>';
			if ( $i < $count - 1 )
				echo $separator;
		}
	}
}	
if ( ! function_exists( 'mts_the_tags' ) ) {
/**
 * Display schema-compliant the_tags()
 *
 * @param string $before
 * @param string $sep
 * @param string $after
 */
	function mts_the_tags($before = '', $sep = ', ', $after = '</div>') {
		if ( empty( $before ) ) {
			$before = '<div class="tags border-bottom">'.__('Tags: ', 'dividend' );
		}

		$tags = get_the_tags();
		if (empty( $tags ) || is_wp_error( $tags ) ) {
			return;
		}
		$tag_links = array();
		foreach ($tags as $tag) {
			$link = get_tag_link($tag->term_id);
			$tag_links[] = '<a href="' . esc_url( $link ) . '" rel="tag">' . $tag->name . '</a>';
		}
		echo $before.join($sep, $tag_links).$after;
	}
}	

if (!function_exists('mts_pagination')) {
	/**
	 * Display the pagination.
	 *
	 * @param string $pages
	 * @param int $range
	 */
	function mts_pagination( $pages = '', $range = 3, $pagenavigation_type = 'mts_pagenavigation_type' ) {
		$mts_options = get_option(MTS_THEME_NAME);
		if (isset($mts_options[$pagenavigation_type]) && $mts_options[$pagenavigation_type] == '1' ) { // numeric pagination
			the_posts_pagination( array(
				'mid_size' => 3,
				'prev_text' => '<i class="fa fa-angle-left"></i>',
				'next_text' => '<i class="fa fa-angle-right"></i>',
			) );
		} else { // traditional or ajax pagination
			?>
<div class="pagination pagination-previous-next">
    <ul>
        <li class="nav-previous"><?php next_posts_link( '<i class="fa fa-angle-left"></i>' ); ?></li>
        <li class="nav-next"><?php previous_posts_link( '<i class="fa fa-angle-right"></i>' ); ?></li>
    </ul>
</div>
<?php
		}
	}
}

if ( ! function_exists( 'mts_cart' ) ) {
	/**
	 * Display the woo-commerce login/register link and the cart.
	 */
	function mts_cart() {
	   if (mts_is_wc_active()) {
	   global $mts_options;
?>
<div class="mts-cart">
    <?php global $woocommerce; ?>
    <span>
        <i class="fa fa-user"></i>
        <?php if ( is_user_logged_in() ) { ?>
        <a href="<?php echo esc_url( get_permalink( get_option('woocommerce_myaccount_page_id') ) ); ?>"
            title="<?php _e('My Account', 'dividend' ); ?>"><?php _e('My Account', 'dividend' ); ?></a>
        <?php }
		else { ?>
        <a href="<?php echo esc_url( get_permalink( get_option('woocommerce_myaccount_page_id') ) ); ?>"
            title="<?php _e('Login / Register', 'dividend' ); ?>"><?php _e('Login ', 'dividend' ); ?></a>
        <?php } ?>
    </span>
    <span>
        <i class="fa fa-shopping-cart"></i> <a class="cart-contents" href="<?php echo esc_url( wc_get_cart_url() ); ?>"
            title="<?php _e('View your shopping cart', 'dividend' ); ?>"><?php echo sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'dividend' ), $woocommerce->cart->cart_contents_count);?>
            - <?php echo $woocommerce->cart->get_cart_total(); ?></a>
    </span>
</div>
<?php }
	}
}

if (!function_exists('mts_related_posts')) {
	/**
	 * Display the related posts.
	 */
	function mts_related_posts() {
		$post_id = get_the_ID();
		$mts_options = get_option(MTS_THEME_NAME);
		//if(!empty($mts_options['mts_related_posts'])) { ?>
<!-- Start Related Posts -->
<?php
			$empty_taxonomy = false;
			if (empty($mts_options['mts_related_posts_taxonomy']) || $mts_options['mts_related_posts_taxonomy'] == 'tags') {
				// related posts based on tags
				$tags = get_the_tags($post_id);
				if (empty($tags)) {
					$empty_taxonomy = true;
				} else {
					$tag_ids = array();
					foreach($tags as $individual_tag) {
						$tag_ids[] = $individual_tag->term_id;
					}
					$args = array( 'tag__in' => $tag_ids,
						'post__not_in' => array($post_id),
						'posts_per_page' => isset( $mts_options['mts_related_postsnum'] ) ? $mts_options['mts_related_postsnum'] : 3,
						'ignore_sticky_posts' => 1,
						'orderby' => 'rand'
					);
				}
			 } else {
				// related posts based on categories
				$categories = get_the_category($post_id);
				if (empty($categories)) {
					$empty_taxonomy = true;
				} else {
					$category_ids = array();
					foreach($categories as $individual_category)
						$category_ids[] = $individual_category->term_id;
					$args = array( 'category__in' => $category_ids,
						'post__not_in' => array($post_id),
						'posts_per_page' => $mts_options['mts_related_postsnum'],
						'ignore_sticky_posts' => 1,
						'orderby' => 'rand'
					);
				}
			 }
			if (!$empty_taxonomy) {
			$my_query = new WP_Query( apply_filters( 'mts_related_posts_query_args', $args, $mts_options['mts_related_posts_taxonomy'] ) ); if( $my_query->have_posts() ) {
				echo '<div class="related-posts">';
				echo '<h4>'.__('What are other people reading?', 'dividend' ).'</h4>';
				echo '<div class="related-posts-wrapper">';
				$posts_per_row = 4;
				$j = 0;
				while( $my_query->have_posts() ) { $my_query->the_post(); ?>
<article class="latestPost excerpt  <?php echo (++$j % $posts_per_row == 0) ? 'last' : ''; ?>">
    <div class="latestPost-layout">
        <a href="<?php echo esc_url( get_the_permalink() ); ?>" title="<?php echo esc_attr( get_the_title() ); ?>" class="post-image post-image-left">
            <?php echo '<div class="featured-thumbnail">'; the_post_thumbnail('dividend-related',array('title' => '')); echo '</div>';
							if (function_exists('wp_review_show_total')) wp_review_show_total(true, 'latestPost-review-wrapper'); ?>
            <div class="article-content">
                <header>
                    <h2 class="title front-view-title"><?php the_title(); ?></h2>
                </header>
            </div>
        </a>
    </div>
</article>
<!--.post.excerpt-->
<?php } echo '</div></div>'; }} wp_reset_postdata(); ?>
<!-- .related-posts -->
<?php //}
	}
}

/*------------[ deals Related Posts ]-------------*/
if (!function_exists('mts_deals_related_posts')) {
	/**
	 * Display the deals related posts.
	 */
	function mts_deals_related_posts() {
		$post_id = get_the_ID();
		$mts_options = get_option(MTS_THEME_NAME);
		$terms = wp_get_object_terms( $post_id,  'mts_categories' );
		if ( empty($terms) || !is_array($terms) ) {
			return;
		}
		$args = array(
			'post_type' => 'deals',
			'posts_per_page' => $mts_options['mts_deals_related_postsnum'],
			'orderby' => 'rand',
			'tax_query' => array(
				array(
					'taxonomy' => 'mts_categories',
					'field' => 'slug',
					'terms' => $terms[0]->slug,
				)
			),
			'post__not_in' => array ($post_id),
		);
		
		$my_query = new WP_Query( $args ); if( $my_query->have_posts() ) {
			echo '<div class="deals-related-posts clearfix">';
			echo '<h4>'.__('Similar offers you might like', 'dividend' ).'</h4>';
			echo '<a href="' . get_post_type_archive_link( 'deals' ) . '" class="btn-archive-link">'.__('View all', 'dividend' ).'</a>';
			echo '<div class="clear">';
			$posts_per_row = 1;
			$j = 0;
			while( $my_query->have_posts() ) { $my_query->the_post(); ?>
<div class="relatedpost-layout">
    <article class="latestPost excerpt deals-related-post <?php echo (++$j % $posts_per_row == 0) ? 'last' : ''; ?>">
        <?php $deals_expiry_date = get_post_meta( get_the_ID(), 'mts_deals_expire'); 
				$deals_no_expiry_date = get_post_meta( get_the_ID(), 'mts_deals_no_expire_date'); 
				$deals_button = get_post_meta( get_the_ID(), 'mts_deals_button', true );
	            $deals_button_url = get_post_meta( get_the_ID(), 'mts_deals_button_url', true ); 
				$deals_featured_text = get_post_meta( get_the_ID(), 'mts_deals_featured_text', true ); ?>
        <a href="<?php echo esc_url( get_the_permalink() ); ?>" title="<?php echo esc_attr( get_the_title() ); ?>">
            <?php echo '<div class="single-thumbnail">'; the_post_thumbnail('dividend-single', array('title' => '')); echo '</div>'; ?>
        </a>
        <div class="content-container">
            <?php if( !empty($deals_expiry_date) && empty($deals_no_expiry_date)) : ?>
            <div class="deals-related-expiry">
                <div class="deals-related-expirydate">
                    <?php
									$now = new DateTime(current_time('mysql'));
									$ref = new DateTime($deals_expiry_date[0]);
									$diff = $now->diff($ref);

									if ( $diff->invert ) {
										if ( $diff->days == 1 ) {
											_e('Expired 1 day ago', 'dividend');
										} elseif ( $diff->days != 0 ) {
											printf(__('Expired %d days ago', 'dividend'), $diff->days);
										} else {
											printf(__('Expired %d hours ago', 'dividend'), $diff->h);
										}
									} else {
										if ( $diff->days == 1 ) {
											_e('Ends in 1 day', 'dividend');
										} elseif ( $diff->days != 0 ) {
											printf(__('Ends in %d days', 'dividend'), $diff->days);
										} else {
											printf(__('Ends in %d hours', 'dividend'), $diff->h);
										}
									} ?>
                </div>
            </div>
            <?php endif; ?>
            <?php if ( isset($deals_no_expiry_date[0]) == 'on' ) : ?>
            <div class="deals-related-expiry"><?php _e('Ongoing', 'dividend' ); ?></div>
            <?php endif; ?>
            <h2 class="title front-view-title"><a href="<?php echo esc_url( get_the_permalink() ); ?>"
                    title="<?php echo esc_attr( get_the_title() ); ?>"><?php the_title(); ?></a></h2>
            <?php if( !empty( $deals_featured_text ) ) : ?>
            <div class="deals-featured">
                <?php echo $deals_featured_text; ?>
            </div>
            <?php endif; ?>
            <div class="posts-more-button">
                <a href="<?php echo esc_url( get_the_permalink() ); ?>" title="<?php echo esc_attr( get_the_title() ); ?>"
                    class="deals-more-button"><?php _e('More Info','dividend'); ?></a>
                <?php if( !empty($deals_button) ) { ?>
                <?php if ( !empty($deals_button_url) ) { ?><a href="<?php echo $deals_button_url; ?>" class="deals-button">
                    <?php } ?><?php echo $deals_button; ?><svg baseProfile="tiny" height="40px" version="1.2" viewBox="0 0 24 24" width="40px"
                        fill="#ffffff" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g id="Layer_1">
                            <path
                                d="M13.293,7.293c-0.391,0.391-0.391,1.023,0,1.414L15.586,11H8c-0.552,0-1,0.448-1,1s0.448,1,1,1h7.586l-2.293,2.293   c-0.391,0.391-0.391,1.023,0,1.414C13.488,16.902,13.744,17,14,17s0.512-0.098,0.707-0.293L19.414,12l-4.707-4.707   C14.316,6.902,13.684,6.902,13.293,7.293z" />
                        </g>
                    </svg>
                    <?php if ( !empty($deals_button_url) ) { ?></a><?php } ?>
                <?php } ?>
            </div>
        </div>
    </article>
    <!--.post.excerpt-->
</div>
<?php } echo '</div></div>'; } wp_reset_postdata();
	}
}

/*------------[ Post Meta Info ]-------------*/
if ( ! function_exists('mts_the_postinfo' ) ) {
	/**
	 * Display the post info block.
	 *
	 * @param string $section
	 */
	function mts_the_postinfo( $section = 'home' ) {
		$mts_options = get_option( MTS_THEME_NAME );
		$opt_key = 'mts_'.$section.'_headline_meta_info';

		if ( isset( $mts_options[ $opt_key ] ) && is_array( $mts_options[ $opt_key ] ) && array_key_exists( 'enabled', $mts_options[ $opt_key ] ) ) {
			$headline_meta_info = $mts_options[ $opt_key ]['enabled'];
		} else {
			$headline_meta_info = array();
		}
		if ( ! empty( $headline_meta_info ) ) { ?>
<div class="post-info">
    <?php foreach( $headline_meta_info as $key => $meta ) { mts_the_postinfo_item( $key ); } ?>
</div>
<?php }
	}
}
if ( ! function_exists('mts_the_postinfo_item' ) ) {
	/**
	 * Display information of an item.
	 * @param $item
	 */
	function mts_the_postinfo_item( $item ) {
		switch ( $item ) {
			case 'author':
			?>
<span class="theauthor"><i class="fa fa-user"></i> <span><?php the_author_posts_link(); ?></span></span>
<?php
			break;
			case 'date':
			?>
<span class="thetime date updated"><i class="fa fa-calendar"></i> <span><?php the_time( get_option( 'date_format' ) ); ?></span></span>
<?php
			break;
			case 'category':
			?>
<span class="thecategory"><i class="fa fa-tags"></i> <?php mts_the_category(', ') ?></span>
<?php
			break;
			case 'comment':
			?>
<span class="thecomment"><i class="fa fa-comments"></i> <a href="<?php echo esc_url( get_comments_link() ); ?>"
        itemprop="interactionCount"><?php comments_number();?></a></span>
<?php
			break;
		}
	}
}

if (!function_exists('mts_social_buttons')) {
	/**
	 * Display the social sharing buttons.
	 */
	function mts_social_buttons() {
		$mts_options = get_option( MTS_THEME_NAME );
		$buttons = array();

		if ( isset( $mts_options['mts_social_buttons'] ) && is_array( $mts_options['mts_social_buttons'] ) && array_key_exists( 'enabled', $mts_options['mts_social_buttons'] ) ) {
			$buttons = $mts_options['mts_social_buttons']['enabled'];
		}

		if ( ! empty( $buttons ) && isset( $mts_options['mts_social_button_layout'] ) ) {
			if( $mts_options['mts_social_button_layout'] == 'modern' ) { ?>
<div class="shareit <?php echo $mts_options['mts_social_button_position']; ?>">
    <?php foreach( $buttons as $key => $button ) { mts_social_modern_button( $key ); } ?>
</div>
<?php }	else { ?>
<div class="shareit <?php echo $mts_options['mts_social_button_position']; ?>">
    <?php foreach( $buttons as $key => $button ) { mts_social_button( $key ); } ?>
</div>
<?php }
		}
	}
}

/*------------[ Header Social Icons ]-------------*/
if (!function_exists('mts_header_buttons')) {
	/**
	 * Display the social sharing buttons.
	 */
	function mts_header_buttons() {
		$mts_options = get_option( MTS_THEME_NAME );
		$buttons = array();

		if ( isset( $mts_options['mts_header_buttons'] ) && is_array( $mts_options['mts_header_buttons'] ) && array_key_exists( 'enabled', $mts_options['mts_header_buttons'] ) ) {
			$buttons = $mts_options['mts_header_buttons']['enabled'];
		}

		if ( ! empty( $buttons ) ) {
		?>
<!-- Start Share Buttons -->
<div class="social-buttons">
    <?php foreach( $buttons as $key => $button ) { mts_header_button( $key ); } ?>
</div>
<!-- end Share Buttons -->
<?php
		}
	}
}

if ( ! function_exists('mts_header_button' ) ) {
	/**
	 * Display network-independent sharing buttons.
	 *
	 * @param $button
	 */
	function mts_header_button( $button ) {
		$mts_options = get_option( MTS_THEME_NAME );
		switch ( $button ) {
			case 'twitter':
			?>
<!-- Twitter -->
<span class="header-item twitterbtn">
    <a class="twitter-follow-button" href="https://twitter.com/<?php echo esc_attr( $mts_options['mts_twitter_username'] ); ?>"
        data-show-screen-name="false"><?php esc_html_e( 'Follow', 'dividend' ); ?></a>
</span>
<?php
			break;
			case 'facebook':
			?>
<!-- Facebook -->
<span class="header-item facebookbtn">
    <div id="fb-root"></div>
    <div class="fb-like" data-send="false" data-layout="button_count" data-width="150" data-show-faces="false"
        data-href="<?php echo esc_attr( $mts_options['mts_facebook_url'] ); ?>"></div>
</span>
<?php
			break;
			case 'gplus':
			?>
<!-- GPlus -->
<span class="header-item gplusbtn">
    <div class="g-plusone" data-size="medium" data-href="<?php echo esc_attr( $mts_options['mts_gplus_url'] ); ?>"></div>
</span>
<?php
			break;
		}
	}
}

if ( ! function_exists('mts_social_button' ) ) {
	/**
	 * Display network-independent sharing buttons.
	 *
	 * @param $button
	 */
	function mts_social_button( $button ) {
		$mts_options = get_option( MTS_THEME_NAME );
		switch ( $button ) {
			case 'facebookshare':
			?>
<!-- Facebook Share-->
<span class="share-item facebooksharebtn">
    <div class="fb-share-button" data-layout="button_count"></div>
</span>
<?php
			break;
			case 'twitter':
			?>
<!-- Twitter -->
<span class="share-item twitterbtn">
    <a href="https://twitter.com/share" class="twitter-share-button"
        data-via="<?php echo esc_attr( $mts_options['mts_twitter_username'] ); ?>"><?php esc_html_e( 'Tweet', 'dividend' ); ?></a>
</span>
<?php
			break;
			case 'gplus':
			?>
<!-- GPlus -->
<span class="share-item gplusbtn">
    <g:plusone size="medium"></g:plusone>
</span>
<?php
			break;
			case 'facebook':
			?>
<!-- Facebook -->
<span class="share-item facebookbtn">
    <div id="fb-root"></div>
    <div class="fb-like" data-send="false" data-layout="button_count" data-width="150" data-show-faces="false"></div>
</span>
<?php
			break;
			case 'pinterest':
			?>
<!-- Pinterest -->
<span class="share-item pinbtn">
    <a href="http://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&media=<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'large' ); echo $thumb['0']; ?>&description=<?php the_title(); ?>"
        class="pin-it-button" count-layout="horizontal"><?php esc_html_e( 'Pin It', 'dividend' ); ?></a>
</span>
<?php
			break;
			case 'linkedin':
			?>
<!--Linkedin -->
<span class="share-item linkedinbtn">
    <script type="IN/Share" data-url="<?php echo esc_url( get_the_permalink() ); ?>"></script>
</span>
<?php
			break;
			case 'stumble':
			?>
<!-- Stumble -->
<span class="share-item stumblebtn">
    <a href="http://www.stumbleupon.com/submit?url=<?php echo urlencode(get_permalink()); ?>&title=<?php the_title(); ?>" class="stumble"
        onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><span
            class="stumble-icon"><i class="fa fa-stumbleupon"></i></span><span class="stumble-text"><?php _e('Share', 'dividend'); ?></span></a>
</span>
<?php
			break;
			case 'reddit':
			?>
<!-- Reddit -->
<span class="share-item reddit">
    <a href="//www.reddit.com/submit"
        onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"> <img
            src="<?php echo get_template_directory_uri().'/images/reddit.png' ?>" alt=<?php _e( 'submit to reddit', 'dividend' ); ?> border="0" /></a>
</span>
<?php
			break;
		}
	}
}

if ( ! function_exists('mts_social_modern_button' ) ) {
	/**
	 * Display network-independent sharing buttons.
	 *
	 * @param $button
	 */
	function mts_social_modern_button( $button ) {
		$mts_options = get_option( MTS_THEME_NAME );
		global $post;
		if( is_single() ){
			$imgUrl = $img = '';
			if ( has_post_thumbnail( $post->ID ) ){
				$img = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'dividend-featuredfull' );
				$imgUrl = $img[0];
			}
		}
		switch ( $button ) {
			case 'facebookshare':
			?>
<!-- Facebook -->
<span class="modern-share-item modern-facebooksharebtn">
    <a href="//www.facebook.com/share.php?m2w&s=100&p[url]=<?php echo urlencode(get_permalink()); ?>&p[images][0]=<?php echo urlencode($imgUrl[0]); ?>&p[title]=<?php echo urlencode(get_the_title()); ?>&u=<?php echo urlencode( get_permalink() ); ?>&t=<?php echo urlencode( get_the_title() ); ?>"
        class="facebook"
        onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i
            class="fa fa-facebook"></i><?php _e('Share', 'dividend'); ?></a>
</span>
<?php
			break;
			case 'twitter':
			?>
<!-- Twitter -->
<span class="modern-share-item modern-twitterbutton">
    <?php $via = '';
					if( $mts_options['mts_twitter_username'] ) {
						$via = '&via='. $mts_options['mts_twitter_username'];
					} ?>
    <a href="https://twitter.com/intent/tweet?original_referer=<?php echo urlencode(get_permalink()); ?>&text=<?php echo get_the_title(); ?>&url=<?php echo urlencode(get_permalink()); ?><?php echo $via; ?>"
        onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i
            class="fa fa-twitter"></i> <?php _e('Tweet', 'dividend'); ?></a>
</span>
<?php
			break;
			case 'gplus':
			?>
<!-- GPlus -->
<span class="modern-share-item modern-gplusbtn">
    <!-- <g:plusone size="medium"></g:plusone> -->
    <a href="//plus.google.com/share?url=<?php echo urlencode(get_permalink()); ?>" class="google-plus"
        onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i
            class="fa fa-google-plus"></i><?php _e('Share', 'dividend'); ?></a>
</span>
<?php
			break;
			case 'facebook':
			?>
<!-- Facebook -->
<span class="modern-share-item facebookbtn">
    <div id="fb-root"></div>
    <div class="fb-like" data-send="false" data-layout="button_count" data-width="150" data-show-faces="false"></div>
</span>
<?php
			break;
			case 'pinterest':
				global $post;
			?>
<!-- Pinterest -->
<?php $pinterestimage = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); ?>
<span class="modern-share-item modern-pinbtn">
    <a href="http://pinterest.com/pin/create/button/?url=<?php echo urlencode(get_permalink($post->ID)); ?>&media=<?php echo $pinterestimage[0]; ?>&description=<?php the_title(); ?>"
        class="pinterest"
        onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i
            class="fa fa-pinterest-p"></i><?php _e('Pin it', 'dividend'); ?></a>
</span>
<?php
			break;
			case 'linkedin':
			?>
<!--Linkedin -->
<span class="modern-share-item modern-linkedinbtn">
    <a href="//www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode(get_permalink()); ?>&title=<?php echo get_the_title(); ?>&source=<?php echo 'url'; ?>"
        onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i
            class="fa fa-linkedin"></i><?php _e('Share', 'dividend'); ?></a>
</span>
<?php
			break;
			case 'stumble':
			?>
<!-- Stumble -->
<span class="modern-share-item modern-stumblebtn">
    <a href="http://www.stumbleupon.com/submit?url=<?php echo urlencode(get_permalink()); ?>&title=<?php the_title(); ?>" class="stumble"
        onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i
            class="fa fa-stumbleupon"></i><?php _e('Stumble', 'dividend'); ?></a>
</span>
<?php
			break;
			case 'reddit':
			?>
<!-- Reddit -->
<span class="modern-share-item modern-reddit">
    <a href="//www.reddit.com/submit"
        onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i
            class="fa fa-reddit-alien"></i><?php _e('Reddit', 'dividend'); ?></a>
</span>
<?php
			break;
		}
	}
}

if ( ! function_exists( 'mts_article_class' ) ) {
	/**
	 * Custom `<article>` class name.
	 */
	function mts_article_class() {
		$mts_options = get_option( MTS_THEME_NAME );
		$class = 'article';

		// sidebar or full width
		if ( mts_custom_sidebar() == 'mts_nosidebar' ) {
			$class = 'ss-full-width';
		}

		echo $class;
	}
}

if ( ! function_exists( 'mts_single_page_class' ) ) {
	/**
	 * Custom `#page` class name.
	 */
	function mts_single_page_class() {
		$class = '';

		if ( is_single() || is_page() ) {

			$class = 'single';

			$header_animation = mts_get_post_header_effect();
			if ( !empty( $header_animation )) $class .= ' '.$header_animation;
		}

		echo $class;
	}
}

if( ! function_exists('get_category_sidebar') ){
    function get_category_sidebar($sidebar_name, $cat_name){
        echo '<aside id="sidebar" class="sidebar c-4-12" role="complementary" itemscope itemtype="http://schema.org/WPSideBar">';
            if ( is_active_sidebar( $sidebar_name )){
                dynamic_sidebar( $sidebar_name );
            }
            else{
                echo '<p class="sidebar-info">'.__('Please add any widget on Post Layout Sidebar: '.$cat_name , 'dividend').'</p>';
            }
        echo '</aside>';
    }    
}

if ( ! function_exists( 'mts_archive_post' ) ) {
	/**
	 * Display a post of specific layout.
	 *
	 * @param string $layout
	 */
	function mts_archive_post( $layout = '' ) {

		$mts_options = get_option(MTS_THEME_NAME); ?>
<a href="<?php echo esc_url( get_the_permalink() ); ?>" title="<?php echo esc_attr( get_the_title() ); ?>" class="post-image post-image-left">
    <?php echo '<div class="featured-thumbnail">'; the_post_thumbnail('dividend-featured',array('title' => '')); echo '</div>'; ?>
    <?php if (function_exists('wp_review_show_total')) wp_review_show_total(true, 'latestPost-review-wrapper'); ?>
</a>
<header>
    <h2 class="title front-view-title"><a href="<?php echo esc_url( get_the_permalink() ); ?>"
            title="<?php echo esc_attr( get_the_title() ); ?>"><?php the_title(); ?></a></h2>
    <?php mts_the_postinfo(); ?>
</header>
<?php }
}

function mts_theme_action( $action = null ) {
    update_option( 'mts__thl', '1' );
    update_option( 'mts__pl', '1' );
}

function mts_theme_activation( $oldtheme_name = null, $oldtheme = null ) {
    // Check for Connect plugin version > 1.4
    if ( class_exists('mts_connection') && defined('MTS_CONNECT_ACTIVE') && MTS_CONNECT_ACTIVE ) {
        return;
    }
     $plugin_path = 'mythemeshop-connect/mythemeshop-connect.php';
    
    // Check if plugin exists
    if ( ! function_exists( 'get_plugins' ) ) {
        require_once ABSPATH . 'wp-admin/includes/plugin.php';
    }
    $plugins = get_plugins();
    if ( ! array_key_exists( $plugin_path, $plugins ) ) {
        // auto-install it
        include_once( ABSPATH . 'wp-admin/includes/misc.php' );
        include_once( ABSPATH . 'wp-admin/includes/file.php' );
        include_once( ABSPATH . 'wp-admin/includes/class-wp-upgrader.php' );
        include_once( ABSPATH . 'wp-admin/includes/plugin-install.php' );
        $skin     = new Automatic_Upgrader_Skin();
        $upgrader = new Plugin_Upgrader( $skin );
        $plugin_file = 'https://www.mythemeshop.com/mythemeshop-connect.zip';
        $result = $upgrader->install( $plugin_file );
        // If install fails then revert to previous theme
        if ( is_null( $result ) || is_wp_error( $result ) || is_wp_error( $skin->result ) ) {
            switch_theme( $oldtheme->stylesheet );
            return false;
        }
    } else {
        // Plugin is already installed, check version
        $ver = isset( $plugins[$plugin_path]['Version'] ) ? $plugins[$plugin_path]['Version'] : '1.0';
         if ( version_compare( $ver, '2.0.5' ) === -1 ) { 
            include_once( ABSPATH . 'wp-admin/includes/misc.php' );
            include_once( ABSPATH . 'wp-admin/includes/file.php' );
            include_once( ABSPATH . 'wp-admin/includes/class-wp-upgrader.php' );
            include_once( ABSPATH . 'wp-admin/includes/plugin-install.php' );
            $skin     = new Automatic_Upgrader_Skin();
            $upgrader = new Plugin_Upgrader( $skin );
            
            add_filter( 'pre_site_transient_update_plugins',  'mts_inject_connect_repo', 10, 2 );
            $result = $upgrader->upgrade( $plugin_path );
            remove_filter( 'pre_site_transient_update_plugins', 'mts_inject_connect_repo' );
            
            // If update fails then revert to previous theme
            if ( is_null( $result ) || is_wp_error( $result ) || is_wp_error( $skin->result ) ) {
                switch_theme( $oldtheme->stylesheet );
                return false;
            }
        }
    }
    $activate = activate_plugin( $plugin_path );
}

function mts_inject_connect_repo( $pre, $transient ) {
    $plugin_file = 'https://www.mythemeshop.com/mythemeshop-connect.zip';
    
    $return = new stdClass();
    $return->response = array();
    $return->response['mythemeshop-connect/mythemeshop-connect.php'] = new stdClass();
    $return->response['mythemeshop-connect/mythemeshop-connect.php']->package = $plugin_file;
    
    return $return;
}

add_action( 'wp_loaded', 'mts_maybe_set_constants' );
function mts_maybe_set_constants() {
    if ( ! defined( 'MTS_THEME_S' ) ) {
        mts_set_theme_constants();
    }
}

add_action( 'init', 'mts_nhp_sections_override', -11 );
function mts_nhp_sections_override() {
    define( 'MTS_THEME_INIT', 1 );
    if ( class_exists('mts_connection') && defined('MTS_CONNECT_ACTIVE') && MTS_CONNECT_ACTIVE ) {
        return;
    }
    if ( ! get_option( MTS_THEME_NAME, false ) ) {
    	return;
    }
    add_filter( 'nhp-opts-sections', '__return_empty_array' );
    add_filter( 'nhp-opts-sections', 'mts_nhp_section_placeholder' );
    add_filter( 'nhp-opts-args', 'mts_nhp_opts_override' );
    add_filter( 'nhp-opts-extra-tabs', '__return_empty_array', 11, 1 );
}

function mts_nhp_section_placeholder( $sections ) {
    $sections[] = array(
        'icon' => 'fa fa-cogs',
        'title' => __('Not Connected', 'dividend' ),
        'desc' => '<p class="description">' . __('You will find all the theme options here after connecting with your MyThemeShop account.', 'dividend' ) . '</p>',
        'fields' => array()
    );
    return $sections;
}

function mts_nhp_opts_override( $opts ) {
    $opts['show_import_export'] = false;
    $opts['show_typography'] = false;
    $opts['show_translate'] = false;
    $opts['show_child_theme_opts'] = false;
    $opts['last_tab'] = 0;
    
    return $opts;
}