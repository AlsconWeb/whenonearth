<?php
/**
 * The template for displaying archive pages.
 *
 * Used for displaying archive-type pages. These views can be further customized by
 * creating a separate template for each one.
 *
 * - author.php (Author archive)
 * - category.php (Category archive)
 * - date.php (Date archive)
 * - tag.php (Tag archive)
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 */
$mts_options = get_option(MTS_THEME_NAME);

get_header(); ?>

<div id="page">
	<div class="<?php mts_article_class(); ?>">
		<div id="content_box">
			<h1 class="postsby">
				<span><?php the_archive_title(); ?></span>
			</h1>
			<p><?php the_archive_description(); ?></p>
			
			<?php 
			// Make sure we have a "Latest Posts" in the list
			$latest_posts_present = false;
			$first_key = '';
			foreach ( $mts_options['mts_featured_categories'] as $k => $section ) {
				if ( ! $first_key ) {
					$first_key = $k;
				}
				if ( $section['mts_featured_category'] == 'latest' ) {
					$latest_posts_present = true;
					break;
				}
			}
			if ( ! $latest_posts_present ) {
				$mts_options['mts_featured_categories'][$first_key]['mts_featured_category'] = 'latest';
			}

			foreach ( $mts_options['mts_featured_categories'] as $section ) {
				$category_id = $section['mts_featured_category'];
				$featured_categories[] = $category_id;
				$posts_num = $section['mts_featured_category_postsnum'];
				$post_color = $section['mts_featured_category_color'];
				if( !empty( $section['mts_home_meta_info_enable'] ) ) {
					$post_author = $section['mts_home_meta_info_enable']['author'];
					$post_category = $section['mts_home_meta_info_enable']['category'];
					$post_time = $section['mts_home_meta_info_enable']['time'];
					$post_comment = $section['mts_home_meta_info_enable']['comment'];
				}
				$layout = isset( $section['mts_featured_category_layout'] ) ? $section['mts_featured_category_layout'] : 'layout-2';
				if ( 'latest' == $category_id ) { ?>
					<div class="article-wrap">
						<div class="article-wrap-layouts">
							<div class="article-inner">
								<?php switch ($layout) {
									case 'layout-1':
										echo '<div class="article-layout-1">'; //Layout 1 having Four Grid Post
										break;

									case 'layout-2':
										$sidebar_used = true;
										echo '<div class="article-layout-2">'; //Layout 2 having 1 Big Post With Sidebar
										echo '<div class="article">';
										break;

									case 'layout-3':
										echo '<div class="article-layout-3">'; //Layout 3 having 1 Big Post
										break;

									case 'layout-4':
										echo '<div class="article-layout-4">'; //Layout 4 having 2 Big Post
										break;

									case 'layout-5':
										echo '<div class="article-layout-5">'; //Layout 5 having 3 Big Post
										break;

									case 'layout-6':
										$sidebar_used6 = true;
										echo '<div class="article-layout-6">'; //Layout 6 having Small Post With Sidebar
										echo '<div class="article">';
										break;	

									case 'layout-7':
										echo '<div class="article-layout-7">'; //Layout 7 having 6 Small Post in a row
										break;
									
									default:
										$sidebar_used = true;
										echo '<div class="article-layout-2">';
										echo '<div class="article">';
										break;
								}

								$j = 1; if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
									<?php get_template_part('home/section', $layout ); ?>
								<?php ++$j; endwhile; endif;

								if ( $layout == 'layout-1' || $layout == 'layout-2' || $layout == 'layout-3' || $layout == 'layout-4' || $layout == 'layout-5' || $layout == 'layout-6' || $layout == 'layout-7' ) {
									if ( $j !== 0 ) { // No pagination if there is no posts
										mts_pagination();
									} ?> 
									</div>
								<?php }

								if ( $layout == 'layout-2' ) {
									$sidebar_used = true;
									$cat_name = __( 'Home Layout 2', 'dividend' );									
									$sidebar_name = sanitize_title( strtolower( 'post-layout-2'.$cat_name ));
                    				get_category_sidebar($sidebar_name, $cat_name); ?>
                    				</div> 
								<?php }						

								if ( $layout == 'layout-6' ) {		
									$sidebar_used6 = true;							
									$cat_name = __( 'Home Layout 6', 'dividend' );							
									$sidebar_name = sanitize_title( strtolower( 'post-layout-6'.$cat_name ));
                    				get_category_sidebar($sidebar_name, $cat_name); ?>
                    				</div> 
								<?php } ?>		
							</div>	<!-- article inner -->				
						</div>	<!-- article-wrap-layouts -->
					</div> <!-- article-wrap -->
				<?php }
				} ?>
		</div>
	</div>
<?php get_footer(); ?>