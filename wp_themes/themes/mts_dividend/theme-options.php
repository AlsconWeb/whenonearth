<?php

defined('ABSPATH') or die;

/*
 *
 * Require the framework class before doing anything else, so we can use the defined urls and dirs
 *
 */
require_once( dirname( __FILE__ ) . '/options/options.php' );

/*
 * 
 * Add support tab
 *
 */
if ( ! defined('MTS_THEME_WHITE_LABEL') || ! MTS_THEME_WHITE_LABEL ) {
	require_once( dirname( __FILE__ ) . '/options/support.php' );
	$mts_options_tab_support = MTS_Options_Tab_Support::get_instance();
}

/*
 *
 * Custom function for filtering the sections array given by theme, good for child themes to override or add to the sections.
 * Simply include this function in the child themes functions.php file.
 *
 * NOTE: the defined constansts for urls, and dir will NOT be available at this point in a child theme, so you must use
 * get_template_directory_uri() if you want to use any of the built in icons
 *
 */
function add_another_section($sections){

	//$sections = array();
	$sections[] = array(
		'title' => __('A Section added by hook', 'dividend' ),
		'desc' => '<p class="description">' . __('This is a section created by adding a filter to the sections array, great to allow child themes, to add/remove sections from the options.', 'dividend' ) . '</p>',
		//all the glyphicons are included in the options folder, so you can hook into them, or link to your own custom ones.
		//You dont have to though, leave it blank for default.
		'icon' => trailingslashit(get_template_directory_uri()).'options/img/glyphicons/glyphicons_062_attach.png',
		//Lets leave this as a blank section, no options just some intro text set above.
		'fields' => array()
	);

	return $sections;

}//function
//add_filter('nhp-opts-sections-twenty_eleven', 'add_another_section');


/*
 *
 * Custom function for filtering the args array given by theme, good for child themes to override or add to the args array.
 *
 */
function change_framework_args($args){

	//$args['dev_mode'] = false;

	return $args;

}//function
//add_filter('nhp-opts-args-twenty_eleven', 'change_framework_args');

/*
 * This is the meat of creating the optons page
 *
 * Override some of the default values, uncomment the args and change the values
 * - no $args are required, but there there to be over ridden if needed.
 *
 *
 */

function setup_framework_options(){
	$args = array();

	//Set it to dev mode to view the class settings/info in the form - default is false
	$args['dev_mode'] = false;
	//Remove the default stylesheet? make sure you enqueue another one all the page will look whack!
	//$args['stylesheet_override'] = true;

	//Add HTML before the form
	//$args['intro_text'] = __('<p>This is the HTML which can be displayed before the form, it isnt required, but more info is always better. Anything goes in terms of markup here, any HTML.</p>', 'dividend' );

	if ( ! MTS_THEME_WHITE_LABEL ) {
		//Setup custom links in the footer for share icons
		$args['share_icons']['twitter'] = array(
			'link' => 'http://twitter.com/mythemeshopteam',
			'title' => __( 'Follow Us on Twitter', 'dividend' ),
			'img' => 'fa fa-twitter-square'
		);
		$args['share_icons']['facebook'] = array(
			'link' => 'http://www.facebook.com/mythemeshop',
			'title' => __( 'Like us on Facebook', 'dividend' ),
			'img' => 'fa fa-facebook-square'
		);
	}

	//Choose to disable the import/export feature
	//$args['show_import_export'] = false;

	//Choose a custom option name for your theme options, the default is the theme name in lowercase with spaces replaced by underscores
	$args['opt_name'] = MTS_THEME_NAME;

	//Custom menu icon
	//$args['menu_icon'] = '';

	//Custom menu title for options page - default is "Options"
	$args['menu_title'] = __('Theme Options', 'dividend' );

	//Custom Page Title for options page - default is "Options"
	$args['page_title'] = __('Theme Options', 'dividend' );

	//Custom page slug for options page (wp-admin/themes.php?page=***) - default is "nhp_theme_options"
	$args['page_slug'] = 'theme_options';

	//Custom page capability - default is set to "manage_options"
	//$args['page_cap'] = 'manage_options';

	//page type - "menu" (adds a top menu section) or "submenu" (adds a submenu) - default is set to "menu"
	//$args['page_type'] = 'submenu';

	//parent menu - default is set to "themes.php" (Appearance)
	//the list of available parent menus is available here: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
	//$args['page_parent'] = 'themes.php';

	//custom page location - default 100 - must be unique or will override other items
	$args['page_position'] = 62;

	//Custom page icon class (used to override the page icon next to heading)
	//$args['page_icon'] = 'icon-themes';

	if ( ! MTS_THEME_WHITE_LABEL ) {
		//Set ANY custom page help tabs - displayed using the new help tab API, show in order of definition
		$args['help_tabs'][] = array(
			'id' => 'nhp-opts-1',
			'title' => __('Support', 'dividend' ),
			'content' => '<p>' . sprintf( __('If you are facing any problem with our theme or theme option panel, head over to our %s.', 'dividend' ), '<a href="http://community.mythemeshop.com/">'. __( 'Support Forums', 'dividend' ) . '</a>' ) . '</p>'
		);
		$args['help_tabs'][] = array(
			'id' => 'nhp-opts-2',
			'title' => __('Earn Money', 'dividend' ),
			'content' => '<p>' . sprintf( __('Earn 70%% commision on every sale by refering your friends and readers. Join our %s.', 'dividend' ), '<a href="http://mythemeshop.com/affiliate-program/">' . __( 'Affiliate Program', 'dividend' ) . '</a>' ) . '</p>'
		);
	}

	//Set the Help Sidebar for the options page - no sidebar by default
	//$args['help_sidebar'] = __('<p>This is the sidebar content, HTML is allowed.</p>', 'dividend' );

	$mts_patterns = array(
		'nobg' => array('img' => NHP_OPTIONS_URL.'img/patterns/nobg.png'),
		'pattern0' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern0.png'),
		'pattern1' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern1.png'),
		'pattern2' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern2.png'),
		'pattern3' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern3.png'),
		'pattern4' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern4.png'),
		'pattern5' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern5.png'),
		'pattern6' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern6.png'),
		'pattern7' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern7.png'),
		'pattern8' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern8.png'),
		'pattern9' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern9.png'),
		'pattern10' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern10.png'),
		'pattern11' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern11.png'),
		'pattern12' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern12.png'),
		'pattern13' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern13.png'),
		'pattern14' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern14.png'),
		'pattern15' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern15.png'),
		'pattern16' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern16.png'),
		'pattern17' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern17.png'),
		'pattern18' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern18.png'),
		'pattern19' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern19.png'),
		'pattern20' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern20.png'),
		'pattern21' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern21.png'),
		'pattern22' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern22.png'),
		'pattern23' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern23.png'),
		'pattern24' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern24.png'),
		'pattern25' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern25.png'),
		'pattern26' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern26.png'),
		'pattern27' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern27.png'),
		'pattern28' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern28.png'),
		'pattern29' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern29.png'),
		'pattern30' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern30.png'),
		'pattern31' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern31.png'),
		'pattern32' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern32.png'),
		'pattern33' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern33.png'),
		'pattern34' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern34.png'),
		'pattern35' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern35.png'),
		'pattern36' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern36.png'),
		'pattern37' => array('img' => NHP_OPTIONS_URL.'img/patterns/pattern37.png'),
		'hbg' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg.png'),
		'hbg2' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg2.png'),
		'hbg3' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg3.png'),
		'hbg4' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg4.png'),
		'hbg5' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg5.png'),
		'hbg6' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg6.png'),
		'hbg7' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg7.png'),
		'hbg8' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg8.png'),
		'hbg9' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg9.png'),
		'hbg10' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg10.png'),
		'hbg11' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg11.png'),
		'hbg12' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg12.png'),
		'hbg13' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg13.png'),
		'hbg14' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg14.png'),
		'hbg15' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg15.png'),
		'hbg16' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg16.png'),
		'hbg17' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg17.png'),
		'hbg18' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg18.png'),
		'hbg19' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg19.png'),
		'hbg20' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg20.png'),
		'hbg21' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg21.png'),
		'hbg22' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg22.png'),
		'hbg23' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg23.png'),
		'hbg24' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg24.png'),
		'hbg25' => array('img' => NHP_OPTIONS_URL.'img/patterns/hbg25.png')
	);

	$sections = array();

	$sections[] = array(
		'icon' => 'fa fa-cogs',
		'title' => __('General Settings', 'dividend' ),
		'desc' => '<p class="description">' . __('This tab contains common setting options which will be applied to the whole theme.', 'dividend' ) . '</p>',
		'fields' => array(
			array(
				'id' => 'mts_logo',
				'type' => 'upload',
				'title' => __('Logo Image', 'dividend' ),
				'sub_desc' => wp_kses( __('Upload your logo using the Upload Button or insert image URL. Recommended <strong>120x52px</strong> size.', 'dividend' ), array( 'strong' => array() ) ),
				'return' => 'id'
			),
			array(
				'id' => 'mts_favicon',
				'type' => 'upload',
				'title' => __('Favicon', 'dividend' ),
				'sub_desc' => sprintf( __('Upload a %s image that will represent your website\'s favicon.', 'dividend' ), '<strong>32 x 32 px</strong>' ),
				'return' => 'id'
			),
			array(
				'id' => 'mts_touch_icon',
				'type' => 'upload',
				'title' => __('Touch icon', 'dividend' ),
				'sub_desc' => sprintf( __('Upload a %s image that will represent your website\'s touch icon for iOS 2.0+ and Android 2.1+ devices.', 'dividend' ), '<strong>152 x 152 px</strong>' ),
				'return' => 'id'
			),
			array(
				'id' => 'mts_metro_icon',
				'type' => 'upload',
				'title' => __('Metro icon', 'dividend' ),
				'sub_desc' => sprintf( __('Upload a %s image that will represent your website\'s IE 10 Metro tile icon.', 'dividend' ), '<strong>144 x 144 px</strong>' ),
				'return' => 'id'
			),
			array(
				'id' => 'mts_feedburner',
				'type' => 'text',
				'title' => __('FeedBurner URL', 'dividend' ),
				'sub_desc' => sprintf( __('Enter your FeedBurner\'s URL here, ex: %s and your main feed (http://example.com/feed) will get redirected to the FeedBurner ID entered here.)', 'dividend' ), '<strong>http://feeds.feedburner.com/mythemeshop</strong>' ),
				'validate' => 'url'
			),
			array(
				'id' => 'mts_header_code',
				'type' => 'textarea',
				'title' => __('Header Code', 'dividend' ),
				'sub_desc' => wp_kses( __('Enter the code which you need to place <strong>before closing &lt;/head&gt; tag</strong>. (ex: Google Webmaster Tools verification, Bing Webmaster Center, BuySellAds Script, Alexa verification etc.)', 'dividend' ), array( 'strong' => array() ) )
			),
			array(
				'id' => 'mts_analytics_code',
				'type' => 'textarea',
				'title' => __('Footer Code', 'dividend' ),
				'sub_desc' => wp_kses( __('Enter the codes which you need to place in your footer. <strong>(ex: Google Analytics, Clicky, STATCOUNTER, Woopra, Histats, etc.)</strong>.', 'dividend' ), array( 'strong' => array() ) )
			),
			array(
				'id' => 'mts_pagenavigation_type',
				'type' => 'radio',
				'title' => __('Pagination Type', 'dividend' ),
				'sub_desc' => __('Select pagination type.', 'dividend' ),
				'options' => array(
					'0'=> __('Next / Previous', 'dividend' ),
					'1' => __('Default Numbered (1 2 3 4...)', 'dividend' ),
					'2' => __( 'AJAX (Load More Button)', 'dividend' ),
					'3' => __( 'AJAX (Auto Infinite Scroll)', 'dividend' )
				),
				'std' => '1'
			),
			array(
				'id' => 'mts_ajax_search',
				'type' => 'button_set',
				'title' => __('AJAX Quick search', 'dividend' ),
				'options' => array( '0' => __( 'Off', 'dividend' ), '1' => __( 'On', 'dividend' ) ),
				'sub_desc' => __('Enable or disable search results appearing instantly below the search form', 'dividend' ),
				'std' => '0'
			),
			array(
				'id' => 'mts_responsive',
				'type' => 'button_set',
				'title' => __('Responsiveness', 'dividend' ),
				'options' => array( '0' => __( 'Off', 'dividend' ), '1' => __( 'On', 'dividend' ) ),
				'sub_desc' => __('MyThemeShop themes are responsive, which means they adapt to tablet and mobile devices, ensuring that your content is always displayed beautifully no matter what device visitors are using. Enable or disable responsiveness using this option.', 'dividend' ),
				'std' => '1'
			),
			array(
				'id' => 'mts_shop_products',
				'type' => 'text',
				'title' => __('No. of Products', 'dividend' ),
				'sub_desc' => __('Enter the total number of products which you want to show on shop page (WooCommerce plugin must be enabled).', 'dividend' ),
				'validate' => 'numeric',
				'std' => '9',
				'class' => 'small-text'
			),
			array(
				'id' => 'mts_static_home',
				'type' => 'button_set',
				'title' => __('Static Homepage', 'dividend' ),
				'options' => array( '0' => __( 'Off', 'dividend' ), '1' => __( 'On', 'dividend' ) ),
				'sub_desc' => __('By default if static page is set then this theme usages Deals page as homepage, if you want to make some other page as static page then enable this option. [default value Off]', 'dividend' ),
				'std' => '0',
				'reset_at_version' => '1.1.0'
			),
		)
	);
	$sections[] = array(
		'icon' => 'fa fa-bolt',
		'title' => __('Performance', 'dividend' ),
		'desc' => '<p class="description">' . __('This tab contains performance-related options which can help speed up your website.', 'dividend' ) . '</p>',
		'fields' => array(
			array(
				'id' => 'mts_prefetching',
				'type' => 'button_set',
				'title' => __('Prefetching', 'dividend' ),
				'options' => array( '0' => __( 'Off', 'dividend' ), '1' => __( 'On', 'dividend' ) ),
				'sub_desc' => __('Enable or disable prefetching. If user is on homepage, then single page will load faster and if user is on single page, homepage will load faster in modern browsers.', 'dividend' ),
				'std' => '0'
			),
			array(
				'id' => 'mts_lazy_load',
				'type' => 'button_set_hide_below',
				'title' => __('Lazy Load', 'dividend' ),
				'options' => array( '0' => __( 'Off', 'dividend' ), '1' => __( 'On', 'dividend' ) ),
				'sub_desc' => __('Delay loading of images outside of viewport, until user scrolls to them.', 'dividend' ),
				'std' => '0',
				'args' => array('hide' => 2)
			),
			array(
				'id' => 'mts_lazy_load_thumbs',
				'type' => 'button_set',
				'title' => __('Lazy load featured images', 'dividend' ),
				'options' => array( '0' => __( 'Off', 'dividend' ), '1' => __( 'On', 'dividend' ) ),
				'sub_desc' => __('Enable or disable Lazy load of featured images across site.', 'dividend' ),
				'std' => '0'
			),
			array(
				'id' => 'mts_lazy_load_content',
				'type' => 'button_set',
				'title' => __('Lazy load post content images', 'dividend' ),
				'options' => array( '0' => __( 'Off', 'dividend' ), '1' => __( 'On', 'dividend' ) ),
				'sub_desc' => __('Enable or disable Lazy load of images inside post/page content.', 'dividend' ),
				'std' => '0'
			),
			array(
				'id' => 'mts_async_js',
				'type' => 'button_set',
				'title' => __('Async JavaScript', 'dividend' ),
				'options' => array( '0' => __( 'Off', 'dividend' ), '1' => __( 'On', 'dividend' ) ),
				'sub_desc' => sprintf( __('Add %s attribute to script tags to improve page download speed.', 'dividend' ), '<code>async</code>' ),
				'std' => '1',
			),
			array(
				'id' => 'mts_remove_ver_params',
				'type' => 'button_set',
				'title' => __('Remove ver parameters', 'dividend' ),
				'options' => array( '0' => __( 'Off', 'dividend' ), '1' => __( 'On', 'dividend' ) ),
				'sub_desc' => sprintf( __('Remove %s parameter from CSS and JS file calls. It may improve speed in some browsers which do not cache files having the parameter.', 'dividend' ), '<code>ver</code>' ),
				'std' => '1',
			),
			array(
				'id' => 'mts_optimize_wc',
				'type' => 'button_set',
				'title' => __('Optimize WooCommerce scripts', 'dividend' ),
				'options' => array( '0' => __( 'Off', 'dividend' ), '1' => __( 'On', 'dividend' ) ),
				'sub_desc' => __('Load WooCommerce scripts and styles only on WooCommerce pages (WooCommerce plugin must be enabled).', 'dividend' ),
				'std' => '1'
			),
			'cache_message' => array(
				'id' => 'mts_cache_message',
				'type' => 'info',
				'title' => __('Use Cache', 'dividend' ),
				// Translators: %1$s = popup link to W3 Total Cache, %2$s = popup link to WP Super Cache
				'desc' => sprintf(
					__('A cache plugin can increase page download speed dramatically. We recommend using %1$s or %2$s.', 'dividend' ),
					'<a href="https://community.mythemeshop.com/tutorials/article/8-make-your-website-load-faster-using-w3-total-cache-plugin/" target="_blank" title="W3 Total Cache">W3 Total Cache</a>',
					'<a href="'.admin_url( 'plugin-install.php?tab=plugin-information&plugin=wp-super-cache&TB_iframe=true&width=772&height=574' ).'" class="thickbox" title="WP Super Cache">WP Super Cache</a>'
				),
			),
		)
	);

	// Hide cache message on multisite or if a chache plugin is active already
	if ( is_multisite() || strstr( join( ';', get_option( 'active_plugins' ) ), 'cache' ) ) {
		unset( $sections[1]['fields']['cache_message'] );
	}

	$sections[] = array(
		'icon' => 'fa fa-adjust',
		'title' => __('Styling Options', 'dividend' ),
		'desc' => '<p class="description">' . __('Control the visual appearance of your theme, such as colors, layout and patterns, from here.', 'dividend' ) . '</p>',
		'fields' => array(
			array(
				'id' => 'mts_color_scheme',
				'type' => 'color',
				'title' => __('Primary Color', 'dividend' ),
				'sub_desc' => __('Set your primary color from here', 'dividend' ),
				'std' => '#23282d'
			),
			array(
				'id' => 'mts_secondary_color_scheme',
				'type' => 'color',
				'title' => __('Secondary Color', 'dividend' ),
				'sub_desc' => __('Set your secondary color from here', 'dividend' ),
				'std' => '#ff8454'
			),
			array(
				'id' => 'mts_layout',
				'type' => 'radio_img',
				'title' => __('Layout Style', 'dividend' ),
				'sub_desc' => wp_kses( __('Choose the <strong>default sidebar position</strong> for your site. The position of the sidebar for individual posts can be set in the post editor.', 'dividend' ), array( 'strong' => array() ) ),
				'options' => array(
					'cslayout' => array('img' => NHP_OPTIONS_URL.'img/layouts/cs.png'),
					'sclayout' => array('img' => NHP_OPTIONS_URL.'img/layouts/sc.png')
				),
				'std' => 'cslayout'
			),
			array(
				'id' => 'mts_background',
				'type' => 'background',
				'title' => __('Site Background', 'dividend' ),
				'sub_desc' => __('Set background color, pattern and image from here.', 'dividend' ),
				'options' => array(
					'color'		 => '',
					'image_pattern' => $mts_patterns,
					'image_upload'  => '',
					'repeat'		=> array(),
					'attachment'	=> array(),
					'position'	=> array(),
					'size'		=> array(),
					'gradient'	=> '',
					'parallax'	=> array(),
				),
				'std' => array(
					'color'		 => '#eff0f1',
					'use'		 => 'pattern',
					'image_pattern' => 'nobg',
					'image_upload'  => '',
					'repeat'		=> 'repeat',
					'attachment'	=> 'scroll',
					'position'	=> 'left top',
					'size'		=> 'cover',
					'gradient'	=> array('from' => '#ffffff', 'to' => '#000000', 'direction' => 'horizontal' ),
					'parallax'	=> '0',
				)
			),
			array(
				'id' => 'mts_custom_css',
				'type' => 'textarea',
				'title' => __('Custom CSS', 'dividend' ),
				'sub_desc' => __('You can enter custom CSS code here to further customize your theme. This will override the default CSS used on your site.', 'dividend' )
			),
			array(
				'id' => 'mts_lightbox',
				'type' => 'button_set',
				'title' => __('Lightbox', 'dividend' ),
				'options' => array( '0' => __( 'Off', 'dividend' ), '1' => __( 'On', 'dividend' ) ),
				'sub_desc' => __('A lightbox is a stylized pop-up that allows your visitors to view larger versions of images without leaving the current page. You can enable or disable the lightbox here.', 'dividend' ),
				'std' => '0'
			),
		)
	);
	$sections[] = array(
		'icon' => 'fa fa-credit-card',
		'title' => __('Header', 'dividend' ),
		'desc' => '<p class="description">' . __('From here, you can control the elements of header section.', 'dividend' ) . '</p>',
		'fields' => array(
			array(
				'id' => 'mts_sticky_nav',
				'type' => 'button_set',
				'title' => __('Floating Navigation Menu', 'dividend' ),
				'options' => array( '0' => __( 'Off', 'dividend' ), '1' => __( 'On', 'dividend' ) ),
				'sub_desc' => sprintf( __('Use this button to enable %s.', 'dividend' ), '<strong>' . __('Floating Navigation Menu', 'dividend' ) . '</strong>' ),
				'std' => '0'
			),
			array(
				'id' => 'mts_show_primary_nav',
				'type' => 'button_set',
				'title' => __('Show Primary Menu', 'dividend' ),
				'options' => array( '0' => __( 'Off', 'dividend' ), '1' => __( 'On', 'dividend' ) ),
				'sub_desc' => sprintf( __('Use this button to enable %s.', 'dividend' ), '<strong>' . __( 'Primary Navigation Menu', 'dividend' ) . '</strong>' ),
				'std' => '1'
			),
			array(
				'id' => 'mts_header_section2',
				'type' => 'button_set',
				'title' => __('Show Logo', 'dividend' ),
				'options' => array( '0' => __( 'Off', 'dividend' ), '1' => __( 'On', 'dividend' ) ),
				'sub_desc' => wp_kses( __('Use this button to Show or Hide the <strong>Logo</strong> completely.', 'dividend' ), array( 'strong' => array() ) ),
				'std' => '1'
			),
			array(
				'id' => 'mts_show_login',
				'type' => 'button_set',
				'title' => __('Show Login/Signup Buttons', 'dividend' ),
				'options' => array( '0' => __( 'Off', 'dividend' ), '1' => __( 'On', 'dividend' ) ),
				'sub_desc' => sprintf( __('Use this button to enable %s.', 'dividend' ), '<strong>' . __( 'Login/Signup Buttons', 'dividend' ) . '</strong>' ),
				'std' => '1'
			),
			array(
				'id' => 'mts_header_search',
				'type' => 'button_set',
				'title' => __('Show Header Search', 'dividend' ), 
				'options' => array( '0' => __( 'Off', 'dividend' ), '1' => __( 'On', 'dividend' ) ),
				'sub_desc' => sprintf( __('Use this button to enable %s.', 'dividend' ), '<strong>' . __( 'Header Search', 'dividend' ) . '</strong>' ),
				'std' => '1'
			),
			array(
				'id'	 => 'mts_header_buttons',
				'type'	 => 'layout2',
				'title'	=> __('Header Social Media Buttons', 'dividend' ),
				'sub_desc' => __('Organize how you want your social sharing buttons to appear on Header', 'dividend' ),
				'options'  => array(
					'enabled'  => array(
						'facebook'   => array(
							'label' 	=> __('Facebook', 'dividend' ),
							'subfields'	=> array(
								array(
									'id' => 'mts_facebook_url',
									'type' => 'text',
									'class' => 'medium-text',
									'title' => __('Facebook URL', 'dividend' ) ,
									'sub_desc' => __('Enter Facebook Profile/Page URL here.', 'dividend' ) ,
									'std' => 'https://facebook.com/mythemeshop',
								),
							)
						),
						'twitter'   => array(
							'label' 	=> __('Twitter', 'dividend' ),
							'subfields'	=> array(
								array(
									'id' => 'mts_twitter_username',
									'type' => 'text',
									'class' => 'medium-text',
									'title' => __('Twitter Username', 'dividend' ) ,
									'sub_desc' => __('Enter twitter username here without @', 'dividend' ) ,
									'std' => 'MyThemeShopTeam',
								),
							)
						),
					),
					'disabled' => array(
						'gplus'   => array(
							'label' 	=> __('Google+', 'dividend' ),
							'subfields'	=> array(
								array(
									'id' => 'mts_gplus_url',
									'type' => 'text',
									'class' => 'medium-text',
									'title' => __('Google+ URL', 'dividend' ) ,
									'sub_desc' => __('Enter Google+ Profile/Page URL here.', 'dividend' ) ,
									'std' => 'https://mythemeshop.com/',
								),
							)
						),
					)
				),
				'std'  => array(
					'enabled'  => array(
						'facebook'   => __('Facebook', 'dividend' ),
						'twitter'	 => __('Twitter', 'dividend' ),
					),
					'disabled' => array(
						'gplus'	 => __('Google+', 'dividend' )
					)
				)
			),
			array(
				'id' => 'mts_header_background',
				'type' => 'background',
				'title' => __('Header Background', 'dividend' ),
				'sub_desc' => __('Set header background color, pattern and image from here.', 'dividend' ),
				'options' => array(
					'color'		 => '',
					'image_pattern' => $mts_patterns,
					'image_upload'  => '',
					'repeat'		=> array(),
					'attachment'	=> array(),
					'position'	=> array(),
					'size'		=> array(),
					'gradient'	=> '',
					'parallax'	=> array(),
				),
				'std' => array(
					'color'		 => '#23282d',
					'use'		 => 'pattern',
					'image_pattern' => 'nobg',
					'image_upload'  => '',
					'repeat'		=> 'repeat',
					'attachment'	=> 'scroll',
					'position'	=> 'left top',
					'size'		=> 'cover',
					'gradient'	=> array('from' => '#ffffff', 'to' => '#000000', 'direction' => 'horizontal' ),
					'parallax'	=> '0',
				)
			),
		)
	);
	$sections[] = array(
		'icon' => 'fa fa-table',
		'title' => __('Footer', 'dividend' ),
		'desc' => '<p class="description">' . __('From here, you can control the elements of Footer section.', 'dividend' ) . '</p>',
		'fields' => array(
			array(
				'id' => 'mts_footer_title',
				'type' => 'button_set_hide_below',
				'title' => __('Footer Title', 'dividend' ),
				'sub_desc' => __('Show Footer Title with this option.', 'dividend' ),
				'options' => array( '0' => __( 'Off', 'dividend' ), '1' => __( 'On', 'dividend' ) ),
				'std' => '1',
				'args' => array('hide' => 2)
			),
			array(
				'id' => 'mts_footer_title_text',
				'type' => 'text',
				'title' => __('Footer Title Text', 'dividend' ),
				'sub_desc' => __('Enter Title here, this title will appear just above footer widgets', 'dividend' ),
				'std' => __('the MyThemeShop theme', 'dividend' ),
			),
			array(
				'id' => 'mts_footer_title_color',
				'type' => 'color',
				'title' => __('Footer Title Background', 'dividend' ),
				'sub_desc' => __('Set footer title color from here', 'dividend' ),
				'std' => '#373e46'
			),
			array(
				'id' => 'mts_first_footer',
				'type' => 'button_set_hide_below',
				'title' => __('Footer Widgets', 'dividend' ),
				'sub_desc' => __('Enable or disable footer widget with this option.', 'dividend' ),
				'options' => array( '0' => __( 'Off', 'dividend' ), '1' => __( 'On', 'dividend' ) ),
				'std' => '0'
			),
			array(
				'id' => 'mts_first_footer_num',
				'type' => 'button_set',
				'class' => 'green',
				'title' => __('Footer Layout', 'dividend' ),
				'sub_desc' => wp_kses( __('Choose the number of widget areas in the <strong>footer</strong>', 'dividend' ), array( 'strong' => array() ) ),
				'options' => array(
					'3' => __( '3 Widgets', 'dividend' ),
					'4' => __( '4 Widgets', 'dividend' ),
				),
				'std' => '3'
			),
			array(
				'id' => 'mts_footer_text',
				'type' => 'textarea',
				'title' => __('Footer Disclaimer Text', 'dividend' ),
				'sub_desc' => __('Enter text here, this text will appear just below footer widgets, you can write about the site, disclaimer etc.', 'dividend' ),
				'std' => '<p>Disclaimer: All content on this website is based on individual experience and journalistic research. It does not constitute financial advice. <strong>Mythemeshop</strong> is not liable for how tips are used, nor for content and services on external websites. Common sense should never be neglected!</p><p>We sometimes use affiliated links which may result in a payment following a visitor taking action (such as a purchase or registration) on an external website.</p>',
			),
			array(
				'id' => 'mts_show_footer_nav',
				'type' => 'button_set',
				'title' => __('Show Footer Menu', 'dividend' ),
				'options' => array( '0' => __( 'Off', 'dividend' ), '1' => __( 'On', 'dividend' ) ),
				'sub_desc' => sprintf( __('Use this button to enable %s.', 'dividend' ), '<strong>' . __( 'Footer Navigation Menu', 'dividend' ) . '</strong>' ),
				'std' => '1'
			),
			array(
				'id' => 'mts_move_to_top',
				'type' => 'button_set',
				'title' => __('Show Move To Top Button', 'dividend' ),
				'options' => array( '0' => __( 'Off', 'dividend' ), '1' => __( 'On', 'dividend' ) ),
				'sub_desc' => sprintf( __('Use this button to show or hide move to top button.', 'dividend' ), '<strong>' . __( 'Move To Top button', 'dividend' ) . '</strong>' ),
				'std' => '1'
			),
			array(
				'id' => 'mts_copyrights',
				'type' => 'textarea',
				'title' => __('Copyrights Text', 'dividend' ),
				'sub_desc' => __( 'You can change or remove our link from footer and use your own custom text.', 'dividend' ) . ( MTS_THEME_WHITE_LABEL ? '' : wp_kses( __('(You can also use your affiliate link to <strong>earn 70% of sales</strong>. Ex: <a href="https://mythemeshop.com/go/aff/aff" target="_blank">https://mythemeshop.com/?ref=username</a>)', 'dividend' ), array( 'strong' => array(), 'a' => array( 'href' => array(), 'target' => array() ) ) ) ),
				'std' => MTS_THEME_WHITE_LABEL ? null : sprintf( __( 'Theme by %s', 'dividend' ), '<a href="http://mythemeshop.com/">MyThemeShop</a>' )
			),
			array(
				'id' => 'mts_footer_background',
				'type' => 'background',
				'title' => __('Footer Background', 'dividend' ),
				'sub_desc' => __('Set footer background color, pattern and image from here.', 'dividend' ),
				'options' => array(
					'color'		 => '',
					'image_pattern' => $mts_patterns,
					'image_upload'  => '',
					'repeat'		=> array(),
					'attachment'	=> array(),
					'position'	=> array(),
					'size'		=> array(),
					'gradient'	=> '',
					'parallax'	=> array(),
				),
				'std' => array(
					'color'		 => '#23282d',
					'use'		 => 'pattern',
					'image_pattern' => 'nobg',
					'image_upload'  => '',
					'repeat'		=> 'repeat',
					'attachment'	=> 'scroll',
					'position'	=> 'left top',
					'size'		=> 'cover',
					'gradient'	=> array('from' => '#ffffff', 'to' => '#000000', 'direction' => 'horizontal' ),
					'parallax'	=> '0',
				)
			),
		)
	);

	$sections[] = array(
		'icon' => '',
		'title' => __('Featured Slider', 'dividend'),
		'desc' => '<p class="description">' . __('Control settings related to Featured Slider.', 'dividend' ) . '</p>',
		'fields' => array(
			array(
				'id' => 'mts_full_featured_slider',
				'type' => 'button_set_hide_below',
				'title' => __('Full Featured Posts', 'dividend' ), 
				'options' => array('0' => __('Off','dividend'),'1' => __('On','dividend')),
				'sub_desc' => wp_kses( __('Enable or Disable <strong>Full Featured Slider</strong> section with this button. Featured area will show 4 recent articles from the selected categories.', 'dividend' ), array( 'strong' => array() ) ),
				'std' => '0',
	            'args' => array('hide' => 1)
			),
			array(
				'id' => 'mts_full_featured_slider_cat',
				'type' => 'cats_multi_select',
				'title' => __('Featured Category(s)', 'dividend' ), 
				'sub_desc' => wp_kses( __('Select a category from the drop-down menu, latest articles from this category will be shown in <strong>Featured Slider</strong>.', 'dividend' ), array( 'strong' => array() ) ),
			),			
		),
	);

	$sections[] = array(
		'icon' => '',
		'title' => __('Featured Carousel', 'dividend'),
		'desc' => '<p class="description">' . __('Control settings related to Featured Carousel.', 'dividend' ) . '</p>',
		'fields' => array(
			array(
				'id' => 'mts_featured_carousel',
				'type' => 'button_set_hide_below',
				'title' => __('Featured Carousel', 'dividend' ),
				'options' => array( '0' => __( 'Off', 'dividend' ), '1' => __( 'On', 'dividend' ) ),
				'sub_desc' => wp_kses( __('Enable or Disable <strong>Featured Carousel</strong> with this button. The Carousel will show recent articles from the selected categories.', 'dividend' ), array( 'strong' => array() ) ),
				'std' => '0',
				'args' => array('hide' => 6)
			),
			array(
				'id' => 'mts_featured_carousel_title',
				'type' => 'text',
				'title' => __('Featured Carousel Title', 'dividend' ),
				'sub_desc' => __('Enter featured carousel title from here', 'dividend' ),
				'std' => __('What\'s New?', 'dividend' ),
			),
			array(
				'id' => 'mts_featured_carousel_color',
				'type' => 'color',
				'title' => __('Featured Carousel Color', 'dividend' ),
				'sub_desc' => __('Set featured carousel title background color, slider-controller color etc. from here', 'dividend' ),
				'std' => '#23282d'
			),
			array(
				'id' => 'mts_featured_carousel_cat',
				'type' => 'cats_multi_select',
				'title' => __('Featured Carousel Category(s)', 'dividend' ),
				'sub_desc' => wp_kses( __('Select a category from the drop-down menu, latest articles from this category will be shown <strong>in the featured Carousel</strong>.', 'dividend' ), array( 'strong' => array() ) ),
			),
			array(
				'id' => 'mts_featured_carousel_num',
				'type' => 'text',
				'class' => 'small-text',
				'title' => __('Number of posts', 'dividend' ),
				'sub_desc' => __('Enter the number of posts to show in the featured carousel', 'dividend' ),
				'std' => '4',
				'args' => array('type' => 'number')
			),
			array(
				'id' => 'mts_custom_carousel',
				'type' => 'group',
				'title' => __('Custom Featured Carousel', 'dividend' ),
				'sub_desc' => __('With this option you can set up a carousel with custom image and text instead of the default carousel automatically generated from your posts.', 'dividend' ),
				'groupname' => __('Carousel', 'dividend' ), // Group name
				'subfields' =>
				array(
					array(
						'id' => 'mts_custom_carousel_title',
						'type' => 'text',
						'title' => __('Title', 'dividend' ),
						'sub_desc' => __('Title of the Carousel', 'dividend' ),
					),
					array(
						'id' => 'mts_custom_carousel_image',
						'type' => 'upload',
						'title' => __('Image', 'dividend' ),
						'sub_desc' => __('Upload or select an image for this Carousel', 'dividend' ),
						'return' => 'id'
					),
					array('id' => 'mts_custom_carousel_link',
						'type' => 'text',
						'title' => __('Link', 'dividend' ),
						'sub_desc' => __('Insert a link URL for the Carousel', 'dividend' ),
						'std' => '#'
					),
				),
			),
			array(
				'id' => 'mts_featured_carousel_background',
				'type' => 'background',
				'title' => __('Featured Carousel Background', 'dividend' ),
				'sub_desc' => __('Set featured carousel background color, pattern and image from here.', 'dividend' ),
				'options' => array(
					'color'		 => '',
					'image_pattern' => $mts_patterns,
					'image_upload'  => '',
					'repeat'		=> array(),
					'attachment'	=> array(),
					'position'	=> array(),
					'size'		=> array(),
					'gradient'	=> '',
					'parallax'	=> array(),
				),
				'std' => array(
					'color'		 => '#eff0f1',
					'use'		 => 'pattern',
					'image_pattern' => 'nobg',
					'image_upload'  => '',
					'repeat'		=> 'repeat',
					'attachment'	=> 'scroll',
					'position'	=> 'left top',
					'size'		=> 'cover',
					'gradient'	=> array('from' => '#ffffff', 'to' => '#000000', 'direction' => 'horizontal' ),
					'parallax'	=> '0',
				)
			),
		),
	);

	$sections[] = array(
		'icon' => '',
		'title' => __('Homepage Deals', 'dividend'),
		'desc' => '<p class="description">' . __('Control settings related to Homepage Deals', 'dividend' ) . '</p>',
		'fields' => array(
			array(
				'id' => 'mts_deals_carousel',
				'type' => 'button_set_hide_below',
				'title' => __('Homepage Deals Carousel ', 'dividend' ),
				'options' => array( '0' => __( 'Off', 'dividend' ), '1' => __( 'On', 'dividend' ) ),
				'sub_desc' => wp_kses( __('<strong>Enable or Disable</strong> Homepage Deals carousel with this button. Deals Carousel will show latest articles from the deals.', 'dividend' ), array( 'strong' => array() ) ),
				'std' => '0',
				'args' => array('hide' => 4)
			),
			array(
				'id' => 'mts_deals_background',
				'type' => 'background',
				'title' => __('Deals Background', 'dividend' ),
				'sub_desc' => __('Set deals background color, pattern and image from here.', 'dividend' ),
				'options' => array(
					'color'		 => '',
					'image_pattern' => $mts_patterns,
					'image_upload'  => '',
					'repeat'		=> array(),
					'attachment'	=> array(),
					'position'	=> array(),
					'size'		=> array(),
					'gradient'	=> '',
					'parallax'	=> array(),
				),
				'std' => array(
					'color'		 => '#23282d',
					'use'		 => 'pattern',
					'image_pattern' => 'nobg',
					'image_upload'  => '',
					'repeat'		=> 'repeat',
					'attachment'	=> 'scroll',
					'position'	=> 'left top',
					'size'		=> 'cover',
					'gradient'	=> array('from' => '#ffffff', 'to' => '#000000', 'direction' => 'horizontal' ),
					'parallax'	=> '0',
				)
			),
			array(
				'id' => 'mts_deals_carousel_title',
				'type' => 'text',
				'title' => __( 'Deals Carousel Title', 'dividend' ),
				'sub_desc' => __( 'Enter Deals Carousel Title', 'dividend' ),
				'std' => 'Latest Deals'
			),
			array(
				'id' => 'mts_deals_carousel_color',
				'type' => 'color',
				'title' => __('Deals Title Background Color', 'dividend' ),
				'sub_desc' => __('Set deals carousel title background color from here.', 'dividend' ),
				'std' => '#373e46'
			),
			array(
				'id' => 'mts_deals_count_home',
				'type' => 'text',
				'class' => 'small-text',
				'title' => __('No. of Deals Posts', 'dividend'),
				'sub_desc' => __('Enter the total number of Deals you want to show on homepage deals carousel', 'dividend'),
				'std' => '8',
				'args' => array('type' => 'number')
			),
		),
	);

	$sections[] = array(
		'icon' => '',
		'title' => __('Featured Categories', 'dividend'),
		'desc' => '<p class="description">' . __('Control settings related to Featured Caregories', 'dividend' ) . '</p>',
		'fields' => array(
			array(
				'id' => 'mts_featured_categories',
				'type' => 'group',
				'title'	 => __('Featured Categories', 'dividend' ),
				'sub_desc'  => __('Select categories appearing on the homepage.', 'dividend' ),
				'groupname' => __('Section', 'dividend' ), // Group name
				'subfields' =>
					array(
						array(
							'id' => 'mts_featured_category',
							'type' => 'cats_select',
							'title' => __('Category', 'dividend' ),
							'sub_desc' => __('Select a category or the latest posts for this section', 'dividend' ),
							'std' => 'latest',
							'args' => array('include_latest' => 1, 'hide_empty' => 0),
						),
						array(
							'id' => 'mts_featured_category_layout',
							'type' => 'select',
							'title' => __('Posts Layout', 'dividend'),
							'sub_desc' => __('Select posts layout for this section.', 'dividend'),
							'options' => array(
								'layout-2' => __('Layout 2','dividend'),
								'layout-1' => __('Layout 1','dividend'),
								'layout-3' => __('Layout 3','dividend'),
								'layout-4' => __('Layout 4','dividend'),
								'layout-5' => __('Layout 5','dividend'),
								'layout-6' => __('Layout 6','dividend'),
								'layout-7' => __('Layout 7','dividend'),
							),
							'std' => 'layout-2'
						),
						array(
							'id' => 'mts_featured_category_postsnum',
							'type' => 'text',
							'class' => 'small-text',
							'title' => __('Number of posts', 'dividend' ),
							'sub_desc' => __('Enter the number of posts to show in this section.', 'dividend' ),
							'std' => '5',
							'args' => array('type' => 'number')
						),
						array(
							'id' => 'mts_featured_category_color',
							'type' => 'color',
							'title' => __('Color Scheme', 'dividend' ),
							'sub_desc' => __('Set featured categories title color and posts color from here', 'dividend' ),
							'std' => '#40b3d8'
						),
						array(
							'id' => 'mts_home_meta_info_enable',
							'type' => 'multi_checkbox',
							'title' => __('Layout Post Meta Info', 'dividend' ),
							'sub_desc'  => wp_kses( __('Show or Hide Post Meta Info on Homepage article layout <br />Post Info will be shown in <strong>Large Posts</strong> only', 'dividend' ), array( 'strong' => array(), 'br' => '' ) ),
							'options' => array(
								'author' => __('Author Name', 'dividend' ), 
								'category'=>__('Category', 'dividend' ),
								'time'=> __('Time/Date', 'dividend' ), 
								'comment' => __('Comments', 'dividend' )
							),
							'std' => array(
								'author'=> '0',
								'category'=>'0',
								'time'=> '0',
								'comment'=> '0'
							)
						),
				),
				'std' => array(
					'1' => array(
						'group_title' => '',
						'group_sort' => '1',
						'mts_featured_category' => 'latest',
						'mts_featured_category_color' => '#40b3d8',
						'mts_featured_category_postsnum' => get_option('posts_per_page')
					)
				)
			),
			array(
				'id' => 'mts_post_background',
				'type' => 'background',
				'title' => __('Homepage Post Background', 'dividend' ),
				'sub_desc' => __('Set Homepage Layouts background color, pattern and image from here.', 'dividend' ),
				'options' => array(
					'color'		 => '',
					'image_pattern' => $mts_patterns,
					'image_upload'  => '',
					'repeat'		=> array(),
					'attachment'	=> array(),
					'position'	=> array(),
					'size'		=> array(),
					'gradient'	=> '',
					'parallax'	=> array(),
				),
				'std' => array(
					'color'		 => '#eff0f1',
					'use'		 => 'pattern',
					'image_pattern' => 'nobg',
					'image_upload'  => '',
					'repeat'		=> 'repeat',
					'attachment'	=> 'scroll',
					'position'	=> 'left top',
					'size'		=> 'cover',
					'gradient'	=> array('from' => '#ffffff', 'to' => '#000000', 'direction' => 'horizontal' ),
					'parallax'	=> '0',
				)
			),
		),
	);

	$sections[] = array(
		'icon' => '',
		'title' => __('Call To Action', 'dividend'),
		'desc' => '<p class="description">' . __('Control settings related to Call To Action from here', 'dividend' ) . '</p>',
		'fields' => array(
			array(
				'id' => 'mts_call_to_action',
				'type' => 'button_set_hide_below',
				'title' => __('Call to Action Section', 'dividend' ),
				'options' => array( '0' => __( 'Off', 'dividend' ), '1' => __( 'On', 'dividend' ) ),
				'sub_desc' => wp_kses( __('Enable or Disable <strong>Call To Action</strong> section from here.', 'dividend' ), array( 'strong' => array() ) ),
				'std' => '1',
				'args' => array('hide' => 8)
			),
			array(
				'id' => 'mts_call_to_action_small_text',
				'type' => 'text',
				'title' => __('Small Text', 'dividend' ),
				'sub_desc'=> __('Enter Small Text which will describe type of Call to Action section from here, like categories, tags etc.', 'dividend' ),
				'std'=> 'Competition',
			),
			array(
				'id' => 'mts_call_to_action_title',
				'type' => 'text',
				'title' => __('Call to Action Title', 'dividend' ),
				'sub_desc'=> __('Enter Title of Call to Action section from here.', 'dividend' ),
				'std'=>  wp_kses( __('Win $500 of <span>ASOS Vouchers!</span>', 'dividend' ), array( 'span' => '' ) ),
			),
			array(
				'id' => 'mts_call_to_action_title_highlight_color',
				'type' => 'color',
				'title' => __('Highlight Title Color', 'dividend' ),
				'sub_desc'=>  wp_kses( __('Highlight title color from here, put in span tag <span><strong>Highlight</strong></span>', 'dividend' ), array( 'strong' => array(), 'span' => array() ) ),
				'std' => '#edb746'
			),
			array(
				'id' => 'mts_call_to_action_button_text',
				'type' => 'text',
				'title' => __('Button Text', 'dividend' ),
				'sub_desc' => __('Enter Call To Action button text from here.', 'dividend' ),
				'std' => 'Enter here',
			),
			array(
				'id' => 'mts_call_to_action_button_color',
				'type' => 'color',
				'title' => __('Button Color', 'dividend' ),
				'sub_desc' => __('Set Call To Action button background color from here.', 'dividend' ),
				'std' => '#88bf5f'
			),
			array(
				'id' => 'mts_call_to_action_button_link',
				'type' => 'text',
				'title' => __('Button URL', 'dividend' ),
				'sub_desc' => __('Add Call To Action button URL here.', 'dividend' ),
				'std' => '#'
			),
			array(
				'id' => 'mts_call_to_action_bg',
				'type' => 'background',
				'title' => __('Call to Action Background', 'dividend' ),
				'sub_desc' => __('Set Call To Action background color/pattern/custom image from here', 'dividend' ),
				'options' => array(
					'color'		 => '',
					'image_pattern' => $mts_patterns,
					'image_upload'  => '',
					'repeat'		=> array(),
					'attachment'	=> array(),
					'position'	  => array(),
					'size'		  => array(),
					'gradient'	  => '',
					'parallax'	  => array(),
				),
				'std' => array(
					'color'		 => '#23282d',
					'use'		 	=> 'pattern',
					'image_pattern' => 'nobg',
					'image_upload'  => '',
					'repeat'		=> 'repeat',
					'attachment'	=> 'scroll',
					'position'	  => 'left top',
					'size'		  => 'cover',
					'gradient'	  => array('from' => '#ffffff', 'to' => '#000000', 'direction' => 'horizontal' ),
					'parallax'	  => '0',
				)
			),
			array(
				'id' => 'mts_call_to_action_image',
				'type' => 'upload',
				'title' => __('Right Side Image', 'dividend'),
				'sub_desc' => wp_kses( __('Upload Call To Action Image from here <strong>Recommend Size 586x440px</strong>. This image will be shown on the right side of Call To Action section', 'dividend' ), array( 'strong' => array() ) ),
				'return' => 'url',
				'std' => get_template_directory_uri().'/images/call_to_action.jpg',
			),
		),
	);

	$sections[] = array(
		'icon' => '',
		'title' => __('Clients Carousel', 'dividend'),
		'desc' => '<p class="description">' . __('Control settings related to Clients Carousel.', 'dividend' ) . '</p>',
		'fields' => array(
			array(
				'id' => 'mts_featured_clients',
				'type' => 'button_set_hide_below',
				'title' => __('Clients Carousel', 'dividend' ),
				'options' => array( '0' => __( 'Off', 'dividend' ), '1' => __( 'On', 'dividend' ) ),
				'sub_desc' => __('Use this option to enable or disable Clients Carousel, This Carousel will be shown above Footer', 'dividend' ),
				'std' => '0',
				'args' => array('hide' => 2)
			),
            array(
	            'id'        => 'mts_clients_carousel',
	            'type'      => 'group',
	            'title'     => __('Carousel Items', 'dividend' ),
	            'sub_desc'  => __('With this option you can set up a carousel with custom images and links.', 'dividend' ),
	            'groupname' => __('Carousel', 'dividend' ), // Group name
	            'subfields' => 
	            array(
	                array(
                        'id' => 'mts_clients_carousel_title',
						'type' => 'text',
						'title' => __('Title', 'dividend' ),
						'sub_desc' => __('This title will not be shown', 'dividend' ),
                	),
	                array(
                        'id' => 'mts_clients_carousel_image',
						'type' => 'upload',
						'title' => __('Logo Image', 'dividend' ),
                        'sub_desc' => wp_kses( __('Upload or select a client logo image<strong>(189x137px)</strong> for this client logo carousel', 'dividend' ), array( 'strong' => array() ) ),
                        'return' => 'url'
					),
	                array(
	                	'id' => 'mts_clients_carousel_link',
						'type' => 'text',
						'title' => __('Link', 'dividend' ),
						'sub_desc' => __('Insert a link URL for the carousel', 'dividend' ),
                        'std' => '#'
	                ),
	            ),
	        ),
	        array(
				'id' => 'mts_clients_carousel_bg',
				'type' => 'background',
				'title' => __('Clients Carousel Background', 'dividend' ),
				'sub_desc' => __('Set Clients Carousel background color/pattern/custom image Section from here', 'dividend' ),
				'options' => array(
					'color'		 => '',
					'image_pattern' => $mts_patterns,
					'image_upload'  => '',
					'repeat'		=> array(),
					'attachment'	=> array(),
					'position'	  => array(),
					'size'		  => array(),
					'gradient'	  => '',
					'parallax'	  => array(),
				),
				'std' => array(
					'color'		 => '#ffffff',
					'use'		 	=> 'pattern',
					'image_pattern' => 'nobg',
					'image_upload'  => '',
					'repeat'		=> 'repeat',
					'attachment'	=> 'scroll',
					'position'	  => 'left top',
					'size'		  => 'cover',
					'gradient'	  => array('from' => '#ffffff', 'to' => '#000000', 'direction' => 'horizontal' ),
					'parallax'	  => '0',
				)
			),
		),
	);

	$sections[] = array(
		'icon' => 'fa fa-file-text',
		'title' => __('Single Posts', 'dividend' ),
		'desc' => '<p class="description">' . __('From here, you can control the appearance and functionality of your single posts page.', 'dividend' ) . '</p>',
		'fields' => array(
			array(
				'id' => 'mts_single_post_category',
				'type' => 'button_set',
				'title' => __('Show or Hide Category ', 'dividend' ),
				'options' => array( '0' => __( 'Off', 'dividend' ), '1' => __( 'On', 'dividend' ) ),
				'sub_desc' => __('You can enable or disable single post category from this option', 'dividend' ),
				'std' => '1'
			),
			array(
				'id'	 => 'mts_single_post_layout',
				'type'	 => 'layout2',
				'title'	=> __('Single Post Layout', 'dividend' ),
				'sub_desc' => __('Customize the look of single posts', 'dividend' ),
				'options'  => array(
					'enabled'  => array(
						'content'   => array(
							'label' 	=> __('Post Content', 'dividend' ),
							'subfields'	=> array(
								array(
									'id' => 'mts_single_featured_image',
									'type' => 'button_set',
									'title' => __('Show Featured Image', 'dividend' ), 
									'sub_desc' => __('Use this button to show Featured Image on Single Post', 'dividend' ),
									'options' => array( '0' => __( 'Off', 'dividend' ), '1' => __( 'On', 'dividend' ) ),
									'std' => '1'
								),
							)
						),
						'subscribe-box'   => array(
							'label' 	=> __('Subscription Box', 'dividend' ),
							'subfields'	=> array(
								array(
									'id' => 'mts_subscribe_box_bg',
									'type' => 'background',
									'title' => __('Subscription Box Background', 'dividend' ),
									'sub_desc' => __('Set background color/pattern/custom image for Subscription Box..', 'dividend' ),
									'options' => array(
										'color'		 => '',
										'image_pattern' => $mts_patterns,
										'image_upload'  => 'upload',
										'repeat'		=> array(),
										'attachment'	=> array(),
										'position'	  => array(),
										'size'		  => array(),
										'gradient'	  => '',
										'parallax'	  => array(),
									),
									'std' => array(
										'color'		 => '#23282d',
										'use'		 	=> 'upload',
										'image_pattern' => 'nobg',
										'image_upload'  => get_template_directory_uri().'/images/newsletter-long-bg2.png',
										'repeat'		=> 'no-repeat',
										'attachment'	=> 'scroll',
										'position'	  => 'left top',
										'size'		  => 'cover',
										'gradient'	  => array('from' => '#ffffff', 'to' => '#000000', 'direction' => 'horizontal' ),
										'parallax'	  => '0',
									)
								),
							)
						),
						'related'   => array(
							'label' 	=> __('Related Posts', 'dividend' ),
							'subfields'	=> array(
								array(
									'id' => 'mts_related_posts_taxonomy',
									'type' => 'button_set',
									'title' => __('Related Posts Taxonomy', 'dividend' ) ,
									'options' => array(
										'tags' => __( 'Tags', 'dividend' ),
										'categories' => __( 'Categories', 'dividend' )
									) ,
									'class' => 'green',
									'sub_desc' => __('Related Posts based on tags or categories.', 'dividend' ) ,
									'std' => 'categories'
								),
								array(
									'id' => 'mts_related_postsnum',
									'type' => 'text',
									'class' => 'small-text',
									'title' => __('Number of related posts', 'dividend' ) ,
									'sub_desc' => __('Enter the number of posts to show in the related posts section.', 'dividend' ) ,
									'std' => '4',
									'args' => array(
										'type' => 'number'
									)
								),
							)
						),
						'author'   => array(
							'label' 	=> __('Author Box', 'dividend' ),
							'subfields'	=> array(

							)
						),
					),
					'disabled' => array(
						'tags'   => array(
							'label' 	=> __('Tags', 'dividend' ),
							'subfields'	=> array(
							)
						),
					)
				)
			),
			array(
				'id' => 'mts_single_meta_info_enable',
				'type' => 'multi_checkbox',
				'title' => __('Single Post Meta Info', 'dividend' ),
				'sub_desc' => __('Organize how you want the post meta info to appear on the single post', 'dividend' ),
				'options' => array(
					'author-image' => __('Enable Author Image', 'dividend' ), 
					'author' => __('Enable Author Name', 'dividend' ), 
					'category'=>__('Enable Category', 'dividend' ),
					'time'=> __('Enable Time/Date', 'dividend' ), 
					'comment' => __('Enable Comments', 'dividend' )
				),
				'std' => array(
					'author-image'=> '1',
					'author'=> '1',
					'category'=>'1',
					'time'=> '1',
					'comment'=> '0'
				)
			),
			array(
				'id' => 'mts_breadcrumb',
				'type' => 'button_set',
				'title' => __('Breadcrumbs', 'dividend' ),
				'options' => array( '0' => __( 'Off', 'dividend' ), '1' => __( 'On', 'dividend' ) ),
				'sub_desc' => __('Breadcrumbs are a great way to make your site more user-friendly. You can enable them by checking this box.', 'dividend' ),
				'std' => '0'
			),
			array(
                'id'       => 'mts_comments',
                'type'     => 'layout2',
                'title'    => __('Comments', 'dividend'),
                'sub_desc' => __('Show standard comments, Facebook comments, or both, in tabs layout.', 'dividend'),
                'options'  => array(
                    'enabled'  => array(
                        'comments'   => array(
                        	'label' 	=> __('Comments','dividend'),
                        	'subfields'	=> array()
                        ),
                        'fb_comments'   => array(
                        	'label' 	=> __('Facebook Comments','dividend'),
                        	'subfields'	=> array(
			        			array(
			        				'id' => 'mts_fb_app_id',
			        				'type' => 'text',
			        				'title' => __('Facebook App ID', 'dividend'),
									'sub_desc' => __('Enter your Facebook app ID here. You can create Facebook App id <a href="https://developers.facebook.com/apps" target="_blank">here</a>', 'dividend'),
			        				'class' => 'small'
			        			),
                        	)
                        ),
                    ),
                    'disabled' => array()
                )
            ),
            array(
				'id' => 'mts_like_dislike',
				'type' => 'button_set',
				'title' => __('Like/Dislike on Comments', 'dividend'), 
				'options' => array( '0' => __( 'Off', 'dividend' ), '1' => __( 'On', 'dividend' ) ),
				'sub_desc' => __('Use this button to enable Like &amp; Dislike features for comments post.', 'dividend'),
				'std' => '1'
			),
			array(
				'id' => 'mts_author_comment',
				'type' => 'button_set',
				'title' => __('Highlight Author Comment', 'dividend' ),
				'options' => array( '0' => __( 'Off', 'dividend' ), '1' => __( 'On', 'dividend' ) ),
				'sub_desc' => __('Use this button to highlight author comments.', 'dividend' ),
				'std' => '1'
			),
			array(
				'id' => 'mts_comment_date',
				'type' => 'button_set',
				'title' => __('Date in Comments', 'dividend' ),
				'options' => array( '0' => __( 'Off', 'dividend' ), '1' => __( 'On', 'dividend' ) ),
				'sub_desc' => __('Use this button to show the date for comments.', 'dividend' ),
				'std' => '1'
			),
		)
	);

	$sections[] = array(
			'icon' => 'fa fa-outdent',
			'title' => __('Deals', 'dividend'),
			'desc' => __('Manage the content displayed on the Deals section. You can manage deals posts in the <a target="_blank" href="'.admin_url("edit.php?post_type=deals").'">Deals</a> section of your WordPress dashboard.', 'dividend'),
			'fields' => array(
				array(
					'id' => 'mts_deals_featured_slider',
					'type' => 'button_set_hide_below',
					'title' => __('Deals Featured Posts', 'dividend' ), 
					'options' => array('0' => __('Off','dividend'),'1' => __('On','dividend')),
					'sub_desc' => wp_kses( __('<strong>Enable or Disable</strong> deals featured post with this button. The featured area will show recent 4 articles of deals posts.', 'dividend' ), array( 'strong' => array() ) ),
					'std' => '0',
		            'args' => array('hide' => 1)
				),
				array(
					'id' => 'mts_deals_featured_slider_cat',
					'type' => 'cats_multi_select_custompost',
					'tax' => 'mts_categories',
					'title' => __('Deals Featured Category(s)', 'dividend' ), 
					'sub_desc' => wp_kses( __('Select a category from the drop-down menu, latest articles from this category will be shown <strong>in the deals featured slider</strong>.', 'dividend' ), array( 'strong' => array() ) ),
				),			
				array(
					'id' => 'mts_deals_count',
					'type' => 'text',
					'class' => 'small-text',
					'title' => __( 'No. of Deals to Show', 'dividend' ),
					'sub_desc' => __( 'Enter the number of deals you want to show per page on Deals page', 'dividend' ),
					'std' => '6',
					'args' => array('type' => 'number')
				),
				array(
					'id' => 'mts_deals_exclude_expired',
					'type' => 'button_set',
					'title' => __('Exclude Expired Deals Posts', 'dividend') ,
					'options' => array(
						'0' => __('Off', 'dividend'),
						'1' => __('On', 'dividend'),
					),
					'sub_desc' => __('Setting this option to "On" will hide expired deals posts from search results', 'dividend'),
					'std' => '0',
				),
				array(
					'id' => 'mts_deals_pagenavigation_type',
					'type' => 'radio',
					'title' => __('Pagination Type - Deals page', 'dividend'),
					'sub_desc' => __('Select pagination type for deals page.', 'dividend'),
					'options' => array(
						'0'=> __('Next / Previous', 'dividend'),
						'1' => __('Default Numbered (1 2 3 4...)', 'dividend'),
						'2' => __( 'AJAX (Load More Button)', 'coupon' ),
						'3' => __( 'AJAX (Auto Infinite Scroll)', 'coupon' )
					),
					'std' => '1'
				)
			)
		);

	$sections[] = array(
		'icon' => 'fa fa-file-text',
		'title' => __('Deals Single', 'dividend' ),
		'desc' => '<p class="description">' . __('From here, you can control the appearance and functionality of your Deals single posts page.', 'dividend' ) . '</p>',
		'fields' => array(
			array(
				'id'	 => 'mts_deals_single_post_layout',
				'type'	 => 'layout2',
				'title'	=> __('Deals Single Post Layout', 'dividend' ),
				'sub_desc' => __('Customize the look of Deals single posts', 'dividend' ),
				'options'  => array(
					'enabled'  => array(
						'deals-content'   => array(
							'label' 	=> __('Post Content', 'dividend' ),
							'subfields'	=> array(
								array(
									'id' => 'mts_single_deals_image',
									'type' => 'button_set',
									'title' => __('Show Deals Featured Image', 'dividend' ), 
									'sub_desc' => __('Use this button to show deals featured Image on Deals Single Post', 'dividend' ),
									'options' => array( '0' => __( 'Off', 'dividend' ), '1' => __( 'On', 'dividend' ) ),
									'std' => '1'
								),
							)
						),
						'deals-related'   => array(
							'label' 	=> __('Related Deals', 'dividend' ),
							'subfields'	=> array(
								array(
									'id' => 'mts_deals_related_postsnum',
									'type' => 'text',
									'class' => 'small-text',
									'title' => __('Number of deals related offers', 'dividend' ) ,
									'sub_desc' => __('Enter the number of posts to show in the deals related offers section.', 'dividend' ) ,
									'std' => '3',
									'args' => array(
										'type' => 'number'
									)
								),

							)
						),
						'deals-subscribe'   => array(
							'label' 	=> __('Subscribe Box', 'dividend' ),
							'subfields'	=> array(
							)
						),
					),
					'disabled' => array(
					)
				)
			),
			array(
				'id' => 'mts_deals_single_meta_info_enable',
				'type' => 'multi_checkbox',
				'title' => __('Deals Single Deals Meta Info', 'dividend' ),
				'sub_desc' => __('Organize how you want the post meta info to appear on the deals single', 'dividend' ),
				'options' => array(
					'author-image' => __('Enable Author Image', 'dividend' ), 
					'author' => __('Enable Author Name', 'dividend' ), 
					'category'=>__('Enable Category', 'dividend' ),
					'time'=> __('Enable Time/Date', 'dividend' ), 
					'comment' => __('Enable Comments', 'dividend' )
				),
				'std' => array(
					'author-image'=> '1',
					'author'=> '1',
					'category'=>'1',
					'time'=> '1',
					'comment'=> '0'
				)
			),

		)
	);

	$sections[] = array(
		'icon' => 'fa fa-group',
		'title' => __('Social Buttons', 'dividend' ),
		'desc' => '<p class="description">' . __('Enable or disable social sharing buttons on single posts using these buttons.', 'dividend' ) . '</p>',
		'fields' => array(
			array(
				'id' => 'mts_social_button_layout',
				'type' => 'radio_img',
				'title' => __('Social Sharing Buttons Layout', 'dividend' ),
				'sub_desc' => wp_kses( __('Choose <strong>default</strong> or <strong>modern</strong> layout for social sharing buttons.', 'dividend' ), array( 'strong' => array() ) ),
				'options' => array(
					'default' => array('img' => NHP_OPTIONS_URL.'img/layouts/default-social.jpg'),
					'modern' => array('img' => NHP_OPTIONS_URL.'img/layouts/modern-social.jpg')
				),
				'std' => 'default',
				'reset_at_version' => '1.1.3'
			),
			array(
				'id' => 'mts_social_button_position',
				'type' => 'button_set',
				'title' => __('Social Sharing Buttons Position', 'dividend' ),
				'options' => array('top' => __('Above Content', 'dividend' ), 'bottom' => __('Below Content', 'dividend' ), 'floating' => __('Floating', 'dividend' )),
				'sub_desc'  => wp_kses( __('Choose position for Social Sharing Buttons.<strong> floating option will be shown above 1330px screen size.</strong>', 'dividend' ), array( 'strong' => array() ) ),
				'std' => 'top',
				'class' => 'green'
			),
			array(
				'id' => 'mts_social_buttons_on_pages',
				'type' => 'button_set',
				'title' => __('Social Sharing Buttons on Pages', 'dividend' ),
				'options' => array('0' => __('Off', 'dividend' ), '1' => __('On', 'dividend' )),
				'sub_desc' => __('Enable the sharing buttons for pages too, not just posts.', 'dividend' ),
				'std' => '0',
			),
			array(
				'id'   => 'mts_social_buttons',
				'type' => 'layout',
				'title'	=> __('Social Media Buttons', 'dividend' ),
				'sub_desc' => __('Organize how you want the social sharing buttons to appear on single posts', 'dividend' ),
				'options'  => array(
					'enabled'  => array(
						'facebook'  => __('Facebook Like', 'dividend' ),
						'facebookshare'   => __('Facebook Share', 'dividend' ),
						'twitter'   => __('Twitter', 'dividend' ),
						'gplus' => __('Google Plus', 'dividend' ),
					),
					'disabled' => array(
						'pinterest' => __('Pinterest', 'dividend' ),
						'linkedin'  => __('LinkedIn', 'dividend' ),
						'stumble'   => __('StumbleUpon', 'dividend' ),
						'reddit'   => __('Reddit', 'dividend' ),
					)
				),
				'std'  => array(
					'enabled'  => array(
						'facebook'  => __('Facebook Like', 'dividend' ),
						'facebookshare'   => __('Facebook Share', 'dividend' ),
						'twitter'   => __('Twitter', 'dividend' ),
						'gplus' => __('Google Plus', 'dividend' ),
					),
					'disabled' => array(
						'pinterest' => __('Pinterest', 'dividend' ),
						'linkedin'  => __('LinkedIn', 'dividend' ),
						'stumble'   => __('StumbleUpon', 'dividend' ),
						'reddit'   => __('Reddit', 'dividend' ),
					)
				),
				'reset_at_version' => '1.1.3'
			),
		)
	);
	$sections[] = array(
		'icon' => 'fa fa-bar-chart-o',
		'title' => __('Ad Management', 'dividend' ),
		'desc' => '<p class="description">' . __('Now, ad management is easy with our options panel. You can control everything from here, without using separate plugins.', 'dividend' ) . '</p>',
		'fields' => array(
			array(
				'id' => 'mts_posttop_adcode',
				'type' => 'textarea',
				'title' => __('Below Post Title', 'dividend' ),
				'sub_desc' => __('Paste your Adsense, BSA or other ad code here to show ads below your article title on single posts.', 'dividend' )
			),
			array(
				'id' => 'mts_posttop_adcode_time',
				'type' => 'text',
				'title' => __('Show After X Days', 'dividend' ),
				'sub_desc' => __('Enter the number of days after which you want to show the Below Post Title Ad. Enter 0 to disable this feature.', 'dividend' ),
				'validate' => 'numeric',
				'std' => '0',
				'class' => 'small-text',
				'args' => array('type' => 'number')
			),
			array(
				'id' => 'mts_postend_adcode',
				'type' => 'textarea',
				'title' => __('Below Post Content', 'dividend' ),
				'sub_desc' => __('Paste your Adsense, BSA or other ad code here to show ads below the post content on single posts.', 'dividend' )
			),
			array(
				'id' => 'mts_postend_adcode_time',
				'type' => 'text',
				'title' => __('Show After X Days', 'dividend' ),
				'sub_desc' => __('Enter the number of days after which you want to show the Below Post Title Ad. Enter 0 to disable this feature.', 'dividend' ),
				'validate' => 'numeric',
				'std' => '0',
				'class' => 'small-text',
				'args' => array('type' => 'number')
			),
		)
	);
	$sections[] = array(
		'icon' => 'fa fa-columns',
		'title' => __('Sidebars', 'dividend' ),
		'desc' => '<p class="description">' . __('Now you have full control over the sidebars. Here you can manage sidebars and select one for each section of your site, or select a custom sidebar on a per-post basis in the post editor.', 'dividend' ) . '<br></p>',
		'fields' => array(
			array(
				'id' => 'mts_custom_sidebars',
				'type'  => 'group', //doesn't need to be called for callback fields
				'title' => __('Custom Sidebars', 'dividend' ),
				'sub_desc'  => wp_kses( __('Add custom sidebars. <strong style="font-weight: 800;">You need to save the changes to use the sidebars in the dropdowns below.</strong><br />You can add content to the sidebars in Appearance &gt; Widgets.', 'dividend' ), array( 'strong' => array(), 'br' => array() ) ),
				'groupname' => __('Sidebar', 'dividend' ), // Group name
				'subfields' =>
					array(
						array(
							'id' => 'mts_custom_sidebar_name',
							'type' => 'text',
							'title' => __('Name', 'dividend' ),
							'sub_desc' => __('Example: Homepage Sidebar', 'dividend' )
						),
						array(
							'id' => 'mts_custom_sidebar_id',
							'type' => 'text',
							'title' => __('ID', 'dividend' ),
							'sub_desc' => __('Enter a unique ID for the sidebar. Use only alphanumeric characters, underscores (_) and dashes (-), eg. "sidebar-home"', 'dividend' ),
							'std' => 'sidebar-'
						),
					),
			),
			array(
				'id' => 'mts_sidebar_for_home',
				'type' => 'sidebars_select',
				'title' => __('Homepage', 'dividend' ),
				'sub_desc' => __('Select a sidebar for the homepage.', 'dividend' ),
				'args' => array('allow_nosidebar' => false, 'exclude' => mts_get_excluded_sidebars()),
				'std' => ''
			),
			array(
				'id' => 'mts_sidebar_for_post',
				'type' => 'sidebars_select',
				'title' => __('Single Post', 'dividend' ),
				'sub_desc' => __('Select a sidebar for the single posts. If a post has a custom sidebar set, it will override this.', 'dividend' ),
				'args' => array('exclude' => mts_get_excluded_sidebars()),
				'std' => ''
			),
			array(
				'id' => 'mts_sidebar_for_page',
				'type' => 'sidebars_select',
				'title' => __('Single Page', 'dividend' ),
				'sub_desc' => __('Select a sidebar for the single pages. If a page has a custom sidebar set, it will override this.', 'dividend' ),
				'args' => array('exclude' => mts_get_excluded_sidebars()),
				'std' => ''
			),
			array(
				'id' => 'mts_sidebar_for_archive',
				'type' => 'sidebars_select',
				'title' => __('Archive', 'dividend' ),
				'sub_desc' => __('Select a sidebar for the archives. Specific archive sidebars will override this setting (see below).', 'dividend' ),
				'args' => array('allow_nosidebar' => false, 'exclude' => mts_get_excluded_sidebars()),
				'std' => ''
			),
			array(
				'id' => 'mts_sidebar_for_category',
				'type' => 'sidebars_select',
				'title' => __('Category Archive', 'dividend' ),
				'sub_desc' => __('Select a sidebar for the category archives.', 'dividend' ),
				'args' => array('allow_nosidebar' => false, 'exclude' => mts_get_excluded_sidebars()),
				'std' => ''
			),
			array(
				'id' => 'mts_sidebar_for_tag',
				'type' => 'sidebars_select',
				'title' => __('Tag Archive', 'dividend' ),
				'sub_desc' => __('Select a sidebar for the tag archives.', 'dividend' ),
				'args' => array('allow_nosidebar' => false, 'exclude' => mts_get_excluded_sidebars()),
				'std' => ''
			),
			array(
				'id' => 'mts_sidebar_for_date',
				'type' => 'sidebars_select',
				'title' => __('Date Archive', 'dividend' ),
				'sub_desc' => __('Select a sidebar for the date archives.', 'dividend' ),
				'args' => array('allow_nosidebar' => false, 'exclude' => mts_get_excluded_sidebars()),
				'std' => ''
			),
			array(
				'id' => 'mts_sidebar_for_author',
				'type' => 'sidebars_select',
				'title' => __('Author Archive', 'dividend' ),
				'sub_desc' => __('Select a sidebar for the author archives.', 'dividend' ),
				'args' => array('allow_nosidebar' => false, 'exclude' => mts_get_excluded_sidebars()),
				'std' => ''
			),
			array(
				'id' => 'mts_sidebar_for_search',
				'type' => 'sidebars_select',
				'title' => __('Search', 'dividend' ),
				'sub_desc' => __('Select a sidebar for the search results.', 'dividend' ),
				'args' => array('allow_nosidebar' => false, 'exclude' => mts_get_excluded_sidebars()),
				'std' => ''
			),
			array(
				'id' => 'mts_sidebar_for_notfound',
				'type' => 'sidebars_select',
				'title' => __('404 Error', 'dividend' ),
				'sub_desc' => __('Select a sidebar for the 404 Not found pages.', 'dividend' ),
				'args' => array('allow_nosidebar' => false, 'exclude' => mts_get_excluded_sidebars()),
				'std' => ''
			),
			array(
				'id' => 'mts_sidebar_for_shop',
				'type' => 'sidebars_select',
				'title' => __('Shop Pages', 'dividend' ),
				'sub_desc' => wp_kses( __('Select a sidebar for Shop main page and product archive pages (WooCommerce plugin must be enabled). Default is <strong>Shop Page Sidebar</strong>.', 'dividend' ), array( 'strong' => array() ) ),
				'args' => array('allow_nosidebar' => false, 'exclude' => mts_get_excluded_sidebars()),
				'std' => 'shop-sidebar'
			),
			array(
				'id' => 'mts_sidebar_for_product',
				'type' => 'sidebars_select',
				'title' => __('Single Product', 'dividend' ),
				'sub_desc' => wp_kses( __('Select a sidebar for single products (WooCommerce plugin must be enabled). Default is <strong>Single Product Sidebar</strong>.', 'dividend' ), array( 'strong' => array() ) ),
				'args' => array('allow_nosidebar' => false, 'exclude' => mts_get_excluded_sidebars()),
				'std' => 'product-sidebar'
			),
		),
	);

	$sections[] = array(
		'icon' => 'fa fa-list-alt',
		'title' => __('Navigation', 'dividend' ),
		'desc' => '<p class="description"><div class="controls">' . sprintf( __('Navigation settings can now be modified from the %s.', 'dividend' ), '<a href="nav-menus.php"><b>' . __( 'Menus Section', 'dividend' ) . '</b></a>' ) . '<br></div></p>'
	);


	$tabs = array();

	$args['presets'] = array();
	$args['show_translate'] = false;
	include('theme-presets.php');

	global $NHP_Options;
	$NHP_Options = new NHP_Options($sections, $args, $tabs);

} //function

add_action('init', 'setup_framework_options', 0);

/*
 *
 * Custom function for the callback referenced above
 *
 */
function my_custom_field($field, $value){
	print_r($field);
	print_r($value);

}//function

/*
 *
 * Custom function for the callback validation referenced above
 *
 */
function validate_callback_function($field, $value, $existing_value){

	$error = false;
	$value =  'just testing';
	$return['value'] = $value;
	if($error == true){
		$return['error'] = $field;
	}
	return $return;

}//function

/*--------------------------------------------------------------------
 *
 * Default Font Settings
 *
 --------------------------------------------------------------------*/
if(function_exists('mts_register_typography')) {
	mts_register_typography( array(
		'logo_font' => array(
			'preview_text' => __( 'Logo Font', 'dividend' ),
			'preview_color' => 'dark',
			'font_family' => 'Roboto',
			'font_variant' => '700',
			'font_size' => '30px',
			'font_color' => '#ffffff',
			'css_selectors' => '#logo a'
		),
		'navigation_font' => array(
			'preview_text' => __( 'Navigation Font', 'dividend' ),
			'preview_color' => 'dark',
			'font_family' => 'Roboto',
			'font_variant' => '500',
			'font_size' => '13px',
			'font_color' => '#ffffff',
			'additional_css' => 'text-transform: uppercase; letter-spacing: .95px; ',
			'css_selectors' => '#secondary-navigation a, .login-wrap .user-name, .login-signup a'
		),
		'carousel_title_font' => array(
			'preview_text' => __( 'Carousel Fonts', 'dividend' ),
			'preview_color' => 'light',
			'font_family' => 'Roboto',
			'font_size' => '18px',
			'font_variant' => 'normal',
			'font_color' => '#004aac',
			'css_selectors' => '.carousel-title, .deals-title'
		),
		'home_layout_title_font' => array(
			'preview_text' => __( 'Home Layout Title', 'dividend' ),
			'preview_color' => 'light',
			'font_family' => 'Roboto',
			'font_size' => '18px',
			'font_variant' => 'normal',
			'font_color' => '#004aac',
			'css_selectors' => '.latestPost .title'
		),
		'post_info_font' => array(
			'preview_text' => __( 'Post Info text', 'dividend' ),
			'preview_color' => 'light',
			'font_family' => 'Roboto',
			'font_size' => '15px',
			'font_variant' => '500',
			'font_color' => '#aab2bc',
			'css_selectors' => '.post-info, .breadcrumb, .pagination'
		),
		'call_to_action_font' => array(
			'preview_text' => __( 'Call to Action Font', 'dividend' ),
			'preview_color' => 'light',
			'font_family' => 'Roboto',
			'font_size' => '20px',
			'font_variant' => '700',
			'font_color' => '#fff',
			'css_selectors' => '.call-to-action'
		),
		'single_title_font' => array(
			'preview_text' => __( 'Single Article Title', 'dividend' ),
			'preview_color' => 'light',
			'font_family' => 'Roboto',
			'font_size' => '40px',
			'font_variant' => '700',
			'font_color' => '#000000',
			'css_selectors' => '.single-title, .title.entry-title'
		),
		'deals_title_font' => array(
			'preview_text' => __( 'Deals Posts Title', 'dividend' ),
			'preview_color' => 'light',
			'font_family' => 'Roboto',
			'font_size' => '20px',
			'font_variant' => 'normal',
			'font_color' => '#004aac',
			'css_selectors' => '.deals-related-post.latestPost .title'
		),
		'content_font' => array(
			'preview_text' => __( 'Content Font', 'dividend' ),
			'preview_color' => 'light',
			'font_family' => 'Roboto',
			'font_size' => '16px',
			'font_variant' => 'normal',
			'font_color' => '#111111',
			'css_selectors' => 'body'
		),
		'sidebar_title' => array(
			'preview_text' => __( 'Sidebar Title', 'dividend' ),
			'preview_color' => 'light',
			'font_family' => 'Roboto',
			'font_variant' => '500',
			'font_size' => '16px',
			'font_color' => '#ffffff',
			'additional_css' => 'text-transform: uppercase;',
			'css_selectors' => '.widget h3'
		),
		'sidebar_heading' => array(
			'preview_text' => __( 'Sidebar Post Heading', 'dividend' ),
			'preview_color' => 'light',
			'font_family' => 'Roboto',
			'font_variant' => '500',
			'font_size' => '13px',
			'font_color' => '#004aac',
			'css_selectors' => '.widget .post-title, .sidebar .widget .entry-title, .widget .slide-title, .widget .wpt_comment_meta'
		),
		'sidebar_font' => array(
			'preview_text' => __( 'Sidebar Font', 'dividend' ),
			'preview_color' => 'light',
			'font_family' => 'Roboto',
			'font_variant' => 'normal',
			'font_size' => '14px',
			'font_color' => '#2b2d32',
			'css_selectors' => '.widget'
		),
		'footer_heading' => array(
			'preview_text' => __( 'Footer Heading', 'dividend' ),
			'preview_color' => 'light',
			'font_family' => 'Roboto',
			'font_variant' => '500',
			'font_size' => '14px',
			'font_color' => '#ffffff',
			'additional_css' => 'text-transform: uppercase;',
			'css_selectors' => '#site-footer .widget h3'
		),
		'footer_title_font' => array(
			'preview_text' => __( 'Footer Post Heading', 'dividend' ),
			'preview_color' => 'light',
			'font_family' => 'Roboto',
			'font_variant' => '500',
			'font_size' => '14px',
			'font_color' => '#ffffff',
			'css_selectors' => '#site-footer .widget .post-title, #site-footer .widget .entry-title, #site-footer .widget .slide-title, #site-footer .widget .wpt_comment_meta'
		),
		'footer_font' => array(
			'preview_text' => __( 'Footer Font', 'dividend' ),
			'preview_color' => 'light',
			'font_family' => 'Roboto',
			'font_variant' => 'normal',
			'font_size' => '17px',
			'font_color' => '#ffffff',
			'css_selectors' => '#site-footer .widget'
		),
		'footer_navigation' => array(
			'preview_text' => __( 'Footer Navigation', 'dividend' ),
			'preview_color' => 'light',
			'font_family' => 'Roboto',
			'font_variant' => 'normal',
			'font_size' => '16px',
			'font_color' => '#656e77',
			'css_selectors' => '.footer-navigation li a, .footer-bottom, #site-footer .textwidget, #site-footer .widget_rss li, .footer-navigation li:before'
		),
		'copyright_font' => array(
			'preview_text' => __( 'Copyright Font', 'dividend' ),
			'preview_color' => 'light',
			'font_family' => 'Roboto',
			'font_variant' => 'normal',
			'font_size' => '16px',
			'font_color' => '#ffffff',
			'css_selectors' => '.copyrights'
		),
		'h1_headline' => array(
			'preview_text' => __( 'Content H1', 'dividend' ),
			'preview_color' => 'light',
			'font_family' => 'Roboto',
			'font_variant' => '700',
			'font_size' => '28px',
			'font_color' => '#000000',
		    'css_selectors' => 'h1'
		),
		'h2_headline' => array(
			'preview_text' => __( 'Content H2', 'dividend' ),
			'preview_color' => 'light',
			'font_family' => 'Roboto',
			'font_variant' => '700',
			'font_size' => '24px',
			'font_color' => '#000000',
			'css_selectors' => 'h2'
		),
		'h3_headline' => array(
			'preview_text' => __( 'Content H3', 'dividend' ),
			'preview_color' => 'light',
			'font_family' => 'Roboto',
			'font_variant' => '700',
			'font_size' => '22px',
			'font_color' => '#000000',
			'css_selectors' => 'h3'
		),
		'h4_headline' => array(
			'preview_text' => __( 'Content H4', 'dividend' ),
			'preview_color' => 'light',
			'font_family' => 'Roboto',
			'font_variant' => '700',
			'font_size' => '20px',
			'font_color' => '#000000',
			'css_selectors' => 'h4'
		),
		'h5_headline' => array(
			'preview_text' => __( 'Content H5', 'dividend' ),
			'preview_color' => 'light',
			'font_family' => 'Roboto',
			'font_variant' => '700',
			'font_size' => '18px',
			'font_color' => '#000000',
			'css_selectors' => 'h5'
		),
		'h6_headline' => array(
			'preview_text' => __( 'Content H6', 'dividend' ),
			'preview_color' => 'light',
			'font_family' => 'Roboto',
			'font_variant' => '700',
			'font_size' => '16px',
			'font_color' => '#000000',
			'css_selectors' => 'h6'
		)
	));
}
