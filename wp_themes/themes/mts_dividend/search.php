<?php
/**
 * The template for displaying search results pages.
 */
$mts_options = get_option(MTS_THEME_NAME);

get_header(); ?>

<div id="page">
	<div class="<?php mts_article_class(); ?>">
		<div id="content_box">
			<h1 class="postsby">
				<span><?php _e("Search Results for:", 'dividend' ); ?></span> <?php the_search_query(); ?>
			</h1>
			<div class="article-wrap">
				<div class="article-wrap-layouts">
					<div class="article-inner">
						<div class="article-layout-1">

						<?php $j = 1; if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
							<?php get_template_part('home/section', 'layout-1' ); ?>
						<?php ++$j; endwhile; endif;

						if ( $j !== 0 ) { // No pagination if there is no posts
							mts_pagination();
						} ?> 
						</div>
					</div>	<!-- article inner -->
				</div>	<!-- article-wrap-layouts -->
			</div> <!-- article-wrap -->
		</div>
	</div>
<?php get_footer(); ?>
