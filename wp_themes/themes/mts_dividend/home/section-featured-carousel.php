<?php $mts_options = get_option(MTS_THEME_NAME);
$title = $mts_options['mts_featured_carousel_title']; ?>
<div class="primary-carousel-wrap">
	<div class="container">
		<h3 class="featured-category-title"><?php echo $title; ?></h3>
		<div class="primary-carousel-container clearfix loading">
			<div id="slider" class="primary-carousel">
			<?php if ( empty( $mts_options['mts_custom_carousel'] ) ) { ?>
				<?php
				// prevent implode error
				if ( empty( $mts_options['mts_featured_carousel_cat'] ) || !is_array( $mts_options['mts_featured_carousel_cat'] ) ) {
					$mts_options['mts_featured_carousel_cat'] = array('0');
				}
				$carousel_cat = implode( ",", $mts_options['mts_featured_carousel_cat'] );
				$carousel_query = new WP_Query('cat='.$carousel_cat.'&posts_per_page='.$mts_options['mts_featured_carousel_num']);
				while ( $carousel_query->have_posts() ) : $carousel_query->the_post();
				?>
				<div class="primary-carousel-item"> 
					<a href="<?php echo esc_url( get_the_permalink() ); ?>" class="primary-carousel-post">
						<?php the_post_thumbnail('dividend-carousel',array('title' => '')); ?>
						<div class="carousel-caption">
							<h2 class="carousel-title"><?php echo mts_truncate( get_the_title(), 60 ); ?></h2>
							<p class="thetime date updated"><i class="fa fa-clock-o"></i> <span><?php the_time( get_option( 'date_format' ) ); ?></span></p>
						</div>
					</a> 
				</div>
				<?php endwhile; wp_reset_postdata(); ?>
			<?php } else { ?>
				<?php foreach( $mts_options['mts_custom_carousel'] as $carousel ) : ?>
					<div class="primary-carousel-item">
						<a href="<?php echo esc_url( $carousel['mts_custom_carousel_link'] ); ?>" class="primary-carousel-post">
							<?php echo wp_get_attachment_image( $carousel['mts_custom_carousel_image'], 'dividend-carousel', false, array('title' => '') ); ?>
							<div class="carousel-caption">
								<h2 class="carousel-title"><?php echo esc_html( mts_truncate( $carousel['mts_custom_carousel_title'], 60 ) ) ?></h2>
							</div>
						</a>
					</div>
				<?php endforeach; ?>
			<?php } ?>
			</div><!-- .primary-carousel -->
		</div><!-- .primary-carousel-container -->
	</div>	
</div>