<?php $mts_options = get_option(MTS_THEME_NAME); ?>
<div class="mts-featured-clients">	
	<div class="container">
        <div class="clients-controls">
            <div class="client-title">
                <?php _e('Featured On','dividend'); ?>
            </div>
            <div class="clients-controls-btn">
                <a class="btn client-prev"><i class="fa fa-angle-left"></i></a>
                <a class="btn client-next"><i class="fa fa-angle-right"></i></a>
            </div>
        </div>
        <div class="clients-carousel-container loading">
            <div id="slider" class="clients-carousel">
                <?php foreach( $mts_options['mts_clients_carousel'] as $logo ) : ?>
                    <div class="clients-item">
                        <?php if($logo['mts_clients_carousel_link']) : ?>
                        	<a href="<?php echo esc_url( $logo['mts_clients_carousel_link'] ); ?>">
                        <?php endif; ?>
                            <img src="<?php echo $logo['mts_clients_carousel_image']; ?>" alt="<?php $logo['mts_clients_carousel_title']; ?>" width="189" height="137">
                                <!-- slide-caption -->
                        <?php if($logo['mts_clients_carousel_link']) : ?>
                        	</a>
                    	<?php endif; ?>
                    </div>    	
                <?php endforeach; ?>
            </div><!-- .clients-carousel -->
    	</div><!-- .clients-carousel-container -->
    </div>
</div>	   