<?php $mts_options = get_option(MTS_THEME_NAME); ?>

<?php global $j, $post_color, $post_author, $post_category, $post_time, $post_comment; ?>

<article class="latestPost excerpt <?php echo 'post-' . $j; ?>">
	<div class="latestPost-layout" style="color: <?php echo $post_color; if ($j == 1) { ?> ; background: <?php echo $post_color; ?><?php } ?>; ">
		<a href="<?php echo esc_url( get_the_permalink() ); ?>" title="<?php echo esc_attr( get_the_title() ); ?>" class="post-image post-image-left">
			<?php if ( $j == 1 || $j == 2 ) {
			   echo '<div class="featured-thumbnail">'; the_post_thumbnail('dividend-bigfeatured',array('title' => '')); echo '</div>';
		    } else {
		    	echo '<div class="featured-thumbnail">'; the_post_thumbnail('dividend-featured',array('title' => '')); echo '</div>';
		    }
			if (function_exists('wp_review_show_total')) wp_review_show_total(true, 'latestPost-review-wrapper'); ?>
			<div class="article-content" style="<?php if ($j == 1) { ?> background: <?php echo $post_color; ?><?php } ?>">
				<header>
					<?php if ( $j == 1 || $j == 2 ) { ?>
						<h2 class="title front-view-title"><?php the_title(); ?></h2>
						<div class="post-info">
							<?php if( isset($post_author) == '1' ) { ?>
								<span class="theauthor"><span><?php the_author_meta( 'display_name' ); ?></span></span>
							<?php }
							if( isset($post_category) == '1' ) {
								$category = get_the_category();  
								if(!empty($category)){ ?>
									<span class="thecategory"><?php echo $category[0]->cat_name; ?></span>
								<?php }
							}
							if( isset($post_time) == '1' ) { ?>
								<span class="thetime date updated"><span><?php the_time( get_option( 'date_format' ) ); ?></span></span>
							<?php }
							if( isset($post_comment) == '1' ) { ?>
								<span class="thecomment"><?php comments_number();?></span>
							<?php } ?>
						</div>
					<?php } else { ?>
						<h2 class="title front-view-title"><?php the_title(); ?></h2>
					<?php }	?>
				</header>
			</div>
		</a>
	</div>
</article>